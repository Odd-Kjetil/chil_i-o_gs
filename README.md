Chil-i/o-gs (a horrible name for and AI, we know...)
An AI project, done by:

- Håkon Kleppe Normann
- Odd-Kjetil Aamot Dahl

Based on the source code of mari/o by Seth Bling: 
https://github.com/pakoito/MarI-O/ 

Link to full documentation for the project: 
https://docs.google.com/document/d/1ngsNkohhiH_r-7T0WFQ_5qebH-PNKpobszZoMVHP4zc/edit?usp=sharing

HOW TO RUN CODE ON WINDOWS:

1. Run EmuHawk.exe

2. File->Open ROM
- select Genesis/Sonic the Hedgehog.bin

3. Tools->Lua Console

4. Script->Open Script
-Select chili-ogs/neatEvolveSonicx

-replace x with {1,2,3} depending on what version of the system you would like to run:

1. The plain version, not taking into account the slope of the terrain

2. Takes into account the slope of the terrain

3. Also knows whether or not it is pushing up against an obstacle (best performing agent)

-Press (Draw map) in the window that pops up to see the neural network.
-You can also drag and drop the lua script into the Lua Console window.