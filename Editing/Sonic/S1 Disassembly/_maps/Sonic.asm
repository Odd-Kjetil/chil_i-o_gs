; --------------------------------------------------------------------------------
; Sprite mappings - output from SonMapEd - Sonic 1 format
; --------------------------------------------------------------------------------

SME_z5Jrd:	
		dc.w SME_z5Jrd_B0-SME_z5Jrd, SME_z5Jrd_B1-SME_z5Jrd	
		dc.w SME_z5Jrd_C6-SME_z5Jrd, SME_z5Jrd_D6-SME_z5Jrd	
		dc.w SME_z5Jrd_E6-SME_z5Jrd, SME_z5Jrd_F6-SME_z5Jrd	
		dc.w SME_z5Jrd_106-SME_z5Jrd, SME_z5Jrd_11B-SME_z5Jrd	
		dc.w SME_z5Jrd_126-SME_z5Jrd, SME_z5Jrd_131-SME_z5Jrd	
		dc.w SME_z5Jrd_146-SME_z5Jrd, SME_z5Jrd_151-SME_z5Jrd	
		dc.w SME_z5Jrd_161-SME_z5Jrd, SME_z5Jrd_17B-SME_z5Jrd	
		dc.w SME_z5Jrd_19A-SME_z5Jrd, SME_z5Jrd_1AF-SME_z5Jrd	
		dc.w SME_z5Jrd_1C9-SME_z5Jrd, SME_z5Jrd_1DE-SME_z5Jrd	
		dc.w SME_z5Jrd_1F8-SME_z5Jrd, SME_z5Jrd_20D-SME_z5Jrd	
		dc.w SME_z5Jrd_218-SME_z5Jrd, SME_z5Jrd_223-SME_z5Jrd	
		dc.w SME_z5Jrd_238-SME_z5Jrd, SME_z5Jrd_243-SME_z5Jrd	
		dc.w SME_z5Jrd_253-SME_z5Jrd, SME_z5Jrd_272-SME_z5Jrd	
		dc.w SME_z5Jrd_291-SME_z5Jrd, SME_z5Jrd_2A6-SME_z5Jrd	
		dc.w SME_z5Jrd_2C0-SME_z5Jrd, SME_z5Jrd_2D5-SME_z5Jrd	
		dc.w SME_z5Jrd_2EF-SME_z5Jrd, SME_z5Jrd_2FA-SME_z5Jrd	
		dc.w SME_z5Jrd_305-SME_z5Jrd, SME_z5Jrd_310-SME_z5Jrd	
		dc.w SME_z5Jrd_31B-SME_z5Jrd, SME_z5Jrd_330-SME_z5Jrd	
		dc.w SME_z5Jrd_340-SME_z5Jrd, SME_z5Jrd_355-SME_z5Jrd	
		dc.w SME_z5Jrd_365-SME_z5Jrd, SME_z5Jrd_370-SME_z5Jrd	
		dc.w SME_z5Jrd_37B-SME_z5Jrd, SME_z5Jrd_386-SME_z5Jrd	
		dc.w SME_z5Jrd_391-SME_z5Jrd, SME_z5Jrd_3A6-SME_z5Jrd	
		dc.w SME_z5Jrd_3B1-SME_z5Jrd, SME_z5Jrd_3C6-SME_z5Jrd	
		dc.w SME_z5Jrd_3D1-SME_z5Jrd, SME_z5Jrd_3D7-SME_z5Jrd	
		dc.w SME_z5Jrd_3DD-SME_z5Jrd, SME_z5Jrd_3E3-SME_z5Jrd	
		dc.w SME_z5Jrd_3E9-SME_z5Jrd, SME_z5Jrd_3EF-SME_z5Jrd	
		dc.w SME_z5Jrd_3FA-SME_z5Jrd, SME_z5Jrd_400-SME_z5Jrd	
		dc.w SME_z5Jrd_40B-SME_z5Jrd, SME_z5Jrd_411-SME_z5Jrd	
		dc.w SME_z5Jrd_41C-SME_z5Jrd, SME_z5Jrd_431-SME_z5Jrd	
		dc.w SME_z5Jrd_446-SME_z5Jrd, SME_z5Jrd_456-SME_z5Jrd	
		dc.w SME_z5Jrd_466-SME_z5Jrd, SME_z5Jrd_476-SME_z5Jrd	
		dc.w SME_z5Jrd_481-SME_z5Jrd, SME_z5Jrd_491-SME_z5Jrd	
		dc.w SME_z5Jrd_4A1-SME_z5Jrd, SME_z5Jrd_4B1-SME_z5Jrd	
		dc.w SME_z5Jrd_4C6-SME_z5Jrd, SME_z5Jrd_4DB-SME_z5Jrd	
		dc.w SME_z5Jrd_4F5-SME_z5Jrd, SME_z5Jrd_50F-SME_z5Jrd	
		dc.w SME_z5Jrd_51A-SME_z5Jrd, SME_z5Jrd_52A-SME_z5Jrd	
		dc.w SME_z5Jrd_535-SME_z5Jrd, SME_z5Jrd_545-SME_z5Jrd	
		dc.w SME_z5Jrd_550-SME_z5Jrd, SME_z5Jrd_560-SME_z5Jrd	
		dc.w SME_z5Jrd_570-SME_z5Jrd, SME_z5Jrd_58A-SME_z5Jrd	
		dc.w SME_z5Jrd_5A4-SME_z5Jrd, SME_z5Jrd_5AF-SME_z5Jrd	
		dc.w SME_z5Jrd_5BF-SME_z5Jrd, SME_z5Jrd_5C5-SME_z5Jrd	
		dc.w SME_z5Jrd_5CB-SME_z5Jrd, SME_z5Jrd_5D1-SME_z5Jrd	
		dc.w SME_z5Jrd_5E1-SME_z5Jrd, SME_z5Jrd_5F1-SME_z5Jrd	
		dc.w SME_z5Jrd_601-SME_z5Jrd, SME_z5Jrd_611-SME_z5Jrd	
SME_z5Jrd_B0:	dc.b 0	
SME_z5Jrd_B1:	dc.b 4	
		dc.b $EC, 8, 0, 0, $F0	
		dc.b $F4, $D, 0, 3, $F0	
		dc.b 4, 8, 0, $B, $F0	
		dc.b $C, 8, 0, $E, $F8	
SME_z5Jrd_C6:	dc.b 3	
		dc.b $EC, 9, 0, 0, $F8	
		dc.b $FC, 9, 0, 6, $F8	
		dc.b $C, 8, 0, $C, 0	
SME_z5Jrd_D6:	dc.b 3	
		dc.b $EC, 9, 0, 0, $F0	
		dc.b $FC, 9, 0, 6, $F0	
		dc.b $C, 8, 0, $C, $F8	
SME_z5Jrd_E6:	dc.b 3	
		dc.b $EC, 9, 0, 0, $F0	
		dc.b $FC, 9, 0, 6, $F0	
		dc.b $C, 8, 0, $C, $F8	
SME_z5Jrd_F6:	dc.b 3	
		dc.b $EC, $A, 0, 0, $F0	
		dc.b 4, 8, 0, 9, $F0	
		dc.b $C, 8, 0, $C, $F8	
SME_z5Jrd_106:	dc.b 4	
		dc.b $EB, $D, 0, 0, $EC	
		dc.b $FB, 9, 0, 8, $EC	
		dc.b $FB, 6, 0, $E, 4	
		dc.b $B, 4, 0, $14, $EC	
SME_z5Jrd_11B:	dc.b 2	
		dc.b $EC, $D, 0, 0, $ED	
		dc.b $FC, $E, 0, 8, $F5	
SME_z5Jrd_126:	dc.b 2	
		dc.b $ED, 9, 0, 0, $F3	
		dc.b $FD, $A, 0, 6, $F3	
SME_z5Jrd_131:	dc.b 4	
		dc.b $EB, 9, 0, 0, $F4	
		dc.b $FB, 9, 0, 6, $EC	
		dc.b $FB, 6, 0, $C, 4	
		dc.b $B, 4, 0, $12, $EC	
SME_z5Jrd_146:	dc.b 2	
		dc.b $EC, 9, 0, 0, $F3	
		dc.b $FC, $E, 0, 6, $EB	
SME_z5Jrd_151:	dc.b 3	
		dc.b $ED, $D, 0, 0, $EC	
		dc.b $FD, $C, 0, 8, $F4	
		dc.b 5, 9, 0, $C, $F4	
SME_z5Jrd_161:	dc.b 5	
		dc.b $EB, 9, 0, 0, $EB	
		dc.b $EB, 6, 0, 6, 3	
		dc.b $FB, 8, 0, $C, $EB	
		dc.b 3, 9, 0, $F, $F3	
		dc.b $13, 0, 0, $15, $FB	
SME_z5Jrd_17B:	dc.b 6	
		dc.b $EC, 9, 0, 0, $EC	
		dc.b $EC, 1, 0, 6, 4	
		dc.b $FC, $C, 0, 8, $EC	
		dc.b 4, 9, 0, $C, $F4	
		dc.b $FC, 5, 0, $12, $C	
		dc.b $F4, 0, 0, $16, $14	
SME_z5Jrd_19A:	dc.b 4	
		dc.b $ED, 9, 0, 0, $ED	
		dc.b $ED, 1, 0, 6, 5	
		dc.b $FD, $D, 0, 8, $F5	
		dc.b $D, 8, 0, $10, $FD	
SME_z5Jrd_1AF:	dc.b 5	
		dc.b $EB, 9, 0, 0, $EB	
		dc.b $EB, 5, 0, 6, 3	
		dc.b $FB, $D, 0, $A, $F3	
		dc.b $B, 8, 0, $12, $F3	
		dc.b $13, 4, 0, $15, $FB	
SME_z5Jrd_1C9:	dc.b 4	
		dc.b $EC, 9, 0, 0, $EC	
		dc.b $EC, 1, 0, 6, 4	
		dc.b $FC, $D, 0, 8, $F4	
		dc.b $C, 8, 0, $10, $FC	
SME_z5Jrd_1DE:	dc.b 5	
		dc.b $ED, 9, 0, 0, $ED	
		dc.b $ED, 1, 0, 6, 5	
		dc.b $FD, 0, 0, 8, $ED	
		dc.b $FD, $D, 0, 9, $F5	
		dc.b $D, 8, 0, $11, $FD	
SME_z5Jrd_1F8:	dc.b 4	
		dc.b $F4, 7, 0, 0, $EB	
		dc.b $EC, 9, 0, 8, $FB	
		dc.b $FC, 4, 0, $E, $FB	
		dc.b 4, 9, 0, $10, $FB	
SME_z5Jrd_20D:	dc.b 2	
		dc.b $F4, 7, 0, 0, $EC	
		dc.b $EC, $B, 0, 8, $FC	
SME_z5Jrd_218:	dc.b 2	
		dc.b $F4, 6, 0, 0, $ED	
		dc.b $F4, $A, 0, 6, $FD	
SME_z5Jrd_223:	dc.b 4	
		dc.b $F4, 6, 0, 0, $EB	
		dc.b $EC, 9, 0, 6, $FB	
		dc.b $FC, 4, 0, $C, $FB	
		dc.b 4, 9, 0, $E, $FB	
SME_z5Jrd_238:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EC	
		dc.b $F4, $B, 0, 6, $FC	
SME_z5Jrd_243:	dc.b 3	
		dc.b $F4, 7, 0, 0, $ED	
		dc.b $EC, 0, 0, 8, $FD	
		dc.b $F4, $A, 0, 9, $FD	
SME_z5Jrd_253:	dc.b 6	
		dc.b $FD, 6, 0, 0, $EB	
		dc.b $ED, 4, 0, 6, $F3	
		dc.b $F5, 4, 0, 8, $EB	
		dc.b $F5, $A, 0, $A, $FB	
		dc.b $D, 0, 0, $13, $FB	
		dc.b $FD, 0, 0, $14, $13	
SME_z5Jrd_272:	dc.b 6	
		dc.b $FC, 6, 0, 0, $EC	
		dc.b $E4, 8, 0, 6, $F4	
		dc.b $EC, 4, 0, 9, $FC	
		dc.b $F4, 4, 0, $B, $EC	
		dc.b $F4, $A, 0, $D, $FC	
		dc.b $C, 0, 0, $16, $FC	
SME_z5Jrd_291:	dc.b 4	
		dc.b $FB, 6, 0, 0, $ED	
		dc.b $F3, 4, 0, 6, $ED	
		dc.b $EB, $A, 0, 8, $FD	
		dc.b 3, 4, 0, $11, $FD	
SME_z5Jrd_2A6:	dc.b 5	
		dc.b $FD, 6, 0, 0, $EB	
		dc.b $ED, 8, 0, 6, $F3	
		dc.b $F5, 4, 0, 9, $EB	
		dc.b $F5, $D, 0, $B, $FB	
		dc.b 5, 8, 0, $13, $FB	
SME_z5Jrd_2C0:	dc.b 4	
		dc.b $FC, 6, 0, 0, $EC	
		dc.b $F4, 4, 0, 6, $EC	
		dc.b $EC, $A, 0, 8, $FC	
		dc.b 4, 4, 0, $11, $FC	
SME_z5Jrd_2D5:	dc.b 5	
		dc.b $FB, 6, 0, 0, $ED	
		dc.b $EB, $A, 0, 6, $FD	
		dc.b $F3, 4, 0, $F, $ED	
		dc.b 3, 4, 0, $11, $FD	
		dc.b $B, 0, 0, $13, $FD	
SME_z5Jrd_2EF:	dc.b 2	
		dc.b $EE, 9, 0, 0, $F4	
		dc.b $FE, $E, 0, 6, $EC	
SME_z5Jrd_2FA:	dc.b 2	
		dc.b $EE, 9, 0, 0, $F4	
		dc.b $FE, $E, 0, 6, $EC	
SME_z5Jrd_305:	dc.b 2	
		dc.b $EE, 9, 0, 0, $F4	
		dc.b $FE, $E, 0, 6, $EC	
SME_z5Jrd_310:	dc.b 2	
		dc.b $EE, 9, 0, 0, $F4	
		dc.b $FE, $E, 0, 6, $EC	
SME_z5Jrd_31B:	dc.b 4	
		dc.b $EE, 9, 0, 0, $EE	
		dc.b $EE, 1, 0, 6, 6	
		dc.b $FE, $E, 0, 8, $F6	
		dc.b $FE, 0, 0, $14, $EE	
SME_z5Jrd_330:	dc.b 3	
		dc.b $EE, 9, 0, 0, $EE	
		dc.b $EE, 1, 0, 6, 6	
		dc.b $FE, $E, 0, 8, $F6	
SME_z5Jrd_340:	dc.b 4	
		dc.b $EE, 9, 0, 0, $EE	
		dc.b $EE, 1, 0, 6, 6	
		dc.b $FE, $E, 0, 8, $F6	
		dc.b $FE, 0, 0, $14, $EE	
SME_z5Jrd_355:	dc.b 3	
		dc.b $EE, 9, 0, 0, $EE	
		dc.b $EE, 1, 0, 6, 6	
		dc.b $FE, $E, 0, 8, $F6	
SME_z5Jrd_365:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EE	
		dc.b $F4, $B, 0, 6, $FE	
SME_z5Jrd_370:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EE	
		dc.b $F4, $B, 0, 6, $FE	
SME_z5Jrd_37B:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EE	
		dc.b $F4, $B, 0, 6, $FE	
SME_z5Jrd_386:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EE	
		dc.b $F4, $B, 0, 6, $FE	
SME_z5Jrd_391:	dc.b 4	
		dc.b $FA, 6, 0, 0, $EE	
		dc.b $F2, 4, 0, 6, $EE	
		dc.b $EA, $B, 0, 8, $FE	
		dc.b $A, 0, 0, $14, $FE	
SME_z5Jrd_3A6:	dc.b 2	
		dc.b $F2, 7, 0, 0, $EE	
		dc.b $EA, $B, 0, 8, $FE	
SME_z5Jrd_3B1:	dc.b 4	
		dc.b $FA, 6, 0, 0, $EE	
		dc.b $F2, 4, 0, 6, $EE	
		dc.b $EA, $B, 0, 8, $FE	
		dc.b $A, 0, 0, $14, $FE	
SME_z5Jrd_3C6:	dc.b 2	
		dc.b $F2, 7, 0, 0, $EE	
		dc.b $EA, $B, 0, 8, $FE	
SME_z5Jrd_3D1:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_z5Jrd_3D7:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_z5Jrd_3DD:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_z5Jrd_3E3:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_z5Jrd_3E9:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_z5Jrd_3EF:	dc.b 2	
		dc.b $F4, $E, 0, 0, $EC	
		dc.b $F4, 2, 0, $C, $C	
SME_z5Jrd_3FA:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_z5Jrd_400:	dc.b 2	
		dc.b $EC, $B, 0, 0, $F4	
		dc.b $C, 8, 0, $C, $F4	
SME_z5Jrd_40B:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_z5Jrd_411:	dc.b 2	
		dc.b $ED, 9, 0, 0, $F0	
		dc.b $FD, $E, 0, 6, $F0	
SME_z5Jrd_41C:	dc.b 4	
		dc.b $ED, 9, 0, 0, $F0	
		dc.b $FD, $D, 0, 6, $F0	
		dc.b $D, 4, 0, $E, 0	
		dc.b 5, 0, 0, $10, $E8	
SME_z5Jrd_431:	dc.b 4	
		dc.b $F4, 4, 0, 0, $FC	
		dc.b $FC, $D, 0, 2, $F4	
		dc.b $C, 8, 0, $A, $F4	
		dc.b 4, 0, 0, $D, $EC	
SME_z5Jrd_446:	dc.b 3	
		dc.b $EC, 8, 8, 0, $E8	
		dc.b $F4, 2, 8, 3, 0	
		dc.b $F4, $F, 8, 6, $E0	
SME_z5Jrd_456:	dc.b 3	
		dc.b $EC, $E, 8, 0, $E8	
		dc.b 4, $D, 8, $C, $E0	
		dc.b $C, 0, $18, $14, 0	
SME_z5Jrd_466:	dc.b 3	
		dc.b $F4, $D, 0, 0, $FC	
		dc.b $FC, 5, 0, 8, $EC	
		dc.b 4, 8, 0, $C, $FC	
SME_z5Jrd_476:	dc.b 2	
		dc.b $F4, $A, 0, 0, $E8	
		dc.b $F4, $A, 8, 0, 0	
SME_z5Jrd_481:	dc.b 3	
		dc.b $F4, $D, 0, 0, $E4	
		dc.b $FC, 0, 0, 8, 4	
		dc.b 4, $C, 0, 9, $EC	
SME_z5Jrd_491:	dc.b 3	
		dc.b $F4, $D, 0, 0, $FC	
		dc.b $FC, 5, 0, 8, $EC	
		dc.b 4, 8, 0, $C, $FC	
SME_z5Jrd_4A1:	dc.b 3	
		dc.b $E8, $B, 0, 0, $F0	
		dc.b 8, 4, 0, $C, $F8	
		dc.b $10, 0, 0, $E, $F8	
SME_z5Jrd_4B1:	dc.b 4	
		dc.b $F8, $E, 0, 0, $E8	
		dc.b 0, 5, 0, $C, 8	
		dc.b $F8, 0, 0, $10, 8	
		dc.b $F0, 0, 0, $11, $F8	
SME_z5Jrd_4C6:	dc.b 4	
		dc.b $F8, $E, 0, 0, $E8	
		dc.b 0, 5, 0, $C, 8	
		dc.b $F8, 0, 0, $10, 8	
		dc.b $F0, 0, 0, $11, $F8	
SME_z5Jrd_4DB:	dc.b 5	
		dc.b $E8, $A, 0, 0, $F4	
		dc.b $F0, 1, 0, 9, $C	
		dc.b 0, 9, 0, $B, $F4	
		dc.b $10, 4, 0, $11, $F4	
		dc.b 0, 0, 0, $13, $EC	
SME_z5Jrd_4F5:	dc.b 5	
		dc.b $E8, $A, 0, 0, $F4	
		dc.b $E8, 1, 0, 9, $C	
		dc.b 0, 9, 0, $B, $F4	
		dc.b $10, 4, 0, $11, $F4	
		dc.b 0, 0, 0, $13, $EC	
SME_z5Jrd_50F:	dc.b 2	
		dc.b $ED, $A, 0, 0, $F3	
		dc.b 5, $D, 0, 9, $EB	
SME_z5Jrd_51A:	dc.b 3	
		dc.b $EC, $A, 0, 0, $F3	
		dc.b 4, 8, 0, 9, $F3	
		dc.b $C, 4, 0, $C, $F3	
SME_z5Jrd_52A:	dc.b 2	
		dc.b $ED, $A, 0, 0, $F3	
		dc.b 5, $D, 0, 9, $EB	
SME_z5Jrd_535:	dc.b 3	
		dc.b $EC, $A, 0, 0, $F3	
		dc.b 4, 8, 0, 9, $F3	
		dc.b $C, 4, 0, $C, $F3	
SME_z5Jrd_545:	dc.b 2	
		dc.b $EC, 9, 0, 0, $F0	
		dc.b $FC, $E, 0, 6, $F0	
SME_z5Jrd_550:	dc.b 3	
		dc.b $EC, $A, 0, 0, $F0	
		dc.b 4, 5, 0, 9, $F8	
		dc.b $E4, 0, 0, $D, $F8	
SME_z5Jrd_560:	dc.b 3	
		dc.b $E8, $D, 0, 0, $EC	
		dc.b $E8, 1, 0, 8, $C	
		dc.b $F8, $B, 0, $A, $F4	
SME_z5Jrd_570:	dc.b 5	
		dc.b $E8, $D, 0, 0, $EC	
		dc.b $E8, 1, 0, 8, $C	
		dc.b $F8, 9, 0, $A, $F4	
		dc.b 8, $C, 0, $10, $F4	
		dc.b $10, 0, 0, $14, $F4	
SME_z5Jrd_58A:	dc.b 5	
		dc.b $E8, $D, 0, 0, $EC	
		dc.b $E8, 1, 0, 8, $C	
		dc.b $F8, 9, 0, $A, $F4	
		dc.b 8, $C, 0, $10, $F4	
		dc.b $10, 0, 0, $14, $F4	
SME_z5Jrd_5A4:	dc.b 2	
		dc.b $EC, 8, 0, 0, $F0	
		dc.b $F4, $F, 0, 3, $F0	
SME_z5Jrd_5AF:	dc.b 3	
		dc.b $EC, 8, 0, 0, $F0	
		dc.b $F4, $E, 0, 3, $F0	
		dc.b $C, 8, 0, $F, $F8	
SME_z5Jrd_5BF:	dc.b 1	
		dc.b $F0, $B, 0, 0, $F4	
SME_z5Jrd_5C5:	dc.b 1	
		dc.b $F4, 6, 0, 0, $F8	
SME_z5Jrd_5CB:	dc.b 1	
		dc.b $F8, 1, 0, 0, $FC	
SME_z5Jrd_5D1:	dc.b 3	
		dc.b $F4, $D, 8, 0, $E4	
		dc.b $FC, 5, 8, 8, 4	
		dc.b 4, 8, 8, $C, $EC	
SME_z5Jrd_5E1:	dc.b 3	
		dc.b $F4, $D, 8, 0, $FC	
		dc.b $FC, 0, 8, 8, $F4	
		dc.b 4, $C, 8, 9, $F4	
SME_z5Jrd_5F1:	dc.b 3	
		dc.b $F0, $E, 0, 0, $EC	
		dc.b $F8, 1, 0, $C, $C	
		dc.b 8, $C, 0, $E, $F4	
SME_z5Jrd_601:	dc.b 3	
		dc.b $EB, 9, 0, 0, $F4	
		dc.b $FB, $E, 0, 6, $EC	
		dc.b 3, 1, 0, $12, $C	
SME_z5Jrd_611:	dc.b 2	
		dc.b $F0, $F, 0, 0, $EC	
		dc.b $F8, 2, 0, $10, $C	
		even