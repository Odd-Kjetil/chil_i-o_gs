; --------------------------------------------------------------------------------
; Sprite mappings - output from SonMapEd - Sonic 3 & Knuckles format
; --------------------------------------------------------------------------------

SME_8oSdb:	
		dc.w SME_8oSdb_216-SME_8oSdb, SME_8oSdb_218-SME_8oSdb	
		dc.w SME_8oSdb_22C-SME_8oSdb, SME_8oSdb_240-SME_8oSdb	
		dc.w SME_8oSdb_254-SME_8oSdb, SME_8oSdb_262-SME_8oSdb	
		dc.w SME_8oSdb_276-SME_8oSdb, SME_8oSdb_28A-SME_8oSdb	
		dc.w SME_8oSdb_29E-SME_8oSdb, SME_8oSdb_2AC-SME_8oSdb	
		dc.w SME_8oSdb_2C6-SME_8oSdb, SME_8oSdb_2E0-SME_8oSdb	
		dc.w SME_8oSdb_2FA-SME_8oSdb, SME_8oSdb_314-SME_8oSdb	
		dc.w SME_8oSdb_32E-SME_8oSdb, SME_8oSdb_348-SME_8oSdb	
		dc.w SME_8oSdb_362-SME_8oSdb, SME_8oSdb_37C-SME_8oSdb	
		dc.w SME_8oSdb_390-SME_8oSdb, SME_8oSdb_3A4-SME_8oSdb	
		dc.w SME_8oSdb_3B8-SME_8oSdb, SME_8oSdb_3C6-SME_8oSdb	
		dc.w SME_8oSdb_3DA-SME_8oSdb, SME_8oSdb_3F4-SME_8oSdb	
		dc.w SME_8oSdb_408-SME_8oSdb, SME_8oSdb_416-SME_8oSdb	
		dc.w SME_8oSdb_430-SME_8oSdb, SME_8oSdb_44A-SME_8oSdb	
		dc.w SME_8oSdb_464-SME_8oSdb, SME_8oSdb_47E-SME_8oSdb	
		dc.w SME_8oSdb_498-SME_8oSdb, SME_8oSdb_4B2-SME_8oSdb	
		dc.w SME_8oSdb_4CC-SME_8oSdb, SME_8oSdb_4E6-SME_8oSdb	
		dc.w SME_8oSdb_4F4-SME_8oSdb, SME_8oSdb_502-SME_8oSdb	
		dc.w SME_8oSdb_510-SME_8oSdb, SME_8oSdb_51E-SME_8oSdb	
		dc.w SME_8oSdb_538-SME_8oSdb, SME_8oSdb_546-SME_8oSdb	
		dc.w SME_8oSdb_560-SME_8oSdb, SME_8oSdb_56E-SME_8oSdb	
		dc.w SME_8oSdb_57C-SME_8oSdb, SME_8oSdb_58A-SME_8oSdb	
		dc.w SME_8oSdb_598-SME_8oSdb, SME_8oSdb_5A6-SME_8oSdb	
		dc.w SME_8oSdb_5BA-SME_8oSdb, SME_8oSdb_5C8-SME_8oSdb	
		dc.w SME_8oSdb_5DC-SME_8oSdb, SME_8oSdb_5EA-SME_8oSdb	
		dc.w SME_8oSdb_5FE-SME_8oSdb, SME_8oSdb_618-SME_8oSdb	
		dc.w SME_8oSdb_632-SME_8oSdb, SME_8oSdb_64C-SME_8oSdb	
		dc.w SME_8oSdb_666-SME_8oSdb, SME_8oSdb_67A-SME_8oSdb	
		dc.w SME_8oSdb_688-SME_8oSdb, SME_8oSdb_696-SME_8oSdb	
		dc.w SME_8oSdb_6AA-SME_8oSdb, SME_8oSdb_6C4-SME_8oSdb	
		dc.w SME_8oSdb_6D2-SME_8oSdb, SME_8oSdb_6E0-SME_8oSdb	
		dc.w SME_8oSdb_6F4-SME_8oSdb, SME_8oSdb_6FC-SME_8oSdb	
		dc.w SME_8oSdb_710-SME_8oSdb, SME_8oSdb_72A-SME_8oSdb	
		dc.w SME_8oSdb_744-SME_8oSdb, SME_8oSdb_75E-SME_8oSdb	
		dc.w SME_8oSdb_76C-SME_8oSdb, SME_8oSdb_774-SME_8oSdb	
		dc.w SME_8oSdb_782-SME_8oSdb, SME_8oSdb_78A-SME_8oSdb	
		dc.w SME_8oSdb_798-SME_8oSdb, SME_8oSdb_7AC-SME_8oSdb	
		dc.w SME_8oSdb_7C6-SME_8oSdb, SME_8oSdb_7E0-SME_8oSdb	
		dc.w SME_8oSdb_7F4-SME_8oSdb, SME_8oSdb_808-SME_8oSdb	
		dc.w SME_8oSdb_81C-SME_8oSdb, SME_8oSdb_824-SME_8oSdb	
		dc.w SME_8oSdb_83E-SME_8oSdb, SME_8oSdb_858-SME_8oSdb	
		dc.w SME_8oSdb_866-SME_8oSdb, SME_8oSdb_87A-SME_8oSdb	
		dc.w SME_8oSdb_88E-SME_8oSdb, SME_8oSdb_89C-SME_8oSdb	
		dc.w SME_8oSdb_8B6-SME_8oSdb, SME_8oSdb_8CA-SME_8oSdb	
		dc.w SME_8oSdb_8D8-SME_8oSdb, SME_8oSdb_8E6-SME_8oSdb	
		dc.w SME_8oSdb_900-SME_8oSdb, SME_8oSdb_914-SME_8oSdb	
		dc.w SME_8oSdb_922-SME_8oSdb, SME_8oSdb_936-SME_8oSdb	
		dc.w SME_8oSdb_93E-SME_8oSdb, SME_8oSdb_94C-SME_8oSdb	
		dc.w SME_8oSdb_960-SME_8oSdb, SME_8oSdb_968-SME_8oSdb	
		dc.w SME_8oSdb_976-SME_8oSdb, SME_8oSdb_984-SME_8oSdb	
		dc.w SME_8oSdb_998-SME_8oSdb, SME_8oSdb_9A0-SME_8oSdb	
		dc.w SME_8oSdb_9AE-SME_8oSdb, SME_8oSdb_9C2-SME_8oSdb	
		dc.w SME_8oSdb_9CA-SME_8oSdb, SME_8oSdb_9D8-SME_8oSdb	
		dc.w SME_8oSdb_9E6-SME_8oSdb, SME_8oSdb_A00-SME_8oSdb	
		dc.w SME_8oSdb_A14-SME_8oSdb, SME_8oSdb_A28-SME_8oSdb	
		dc.w SME_8oSdb_A36-SME_8oSdb, SME_8oSdb_A4A-SME_8oSdb	
		dc.w SME_8oSdb_A5E-SME_8oSdb, SME_8oSdb_A72-SME_8oSdb	
		dc.w SME_8oSdb_A80-SME_8oSdb, SME_8oSdb_A9A-SME_8oSdb	
		dc.w SME_8oSdb_AAE-SME_8oSdb, SME_8oSdb_AC2-SME_8oSdb	
		dc.w SME_8oSdb_AD0-SME_8oSdb, SME_8oSdb_AE4-SME_8oSdb	
		dc.w SME_8oSdb_AEC-SME_8oSdb, SME_8oSdb_B06-SME_8oSdb	
		dc.w SME_8oSdb_B20-SME_8oSdb, SME_8oSdb_B34-SME_8oSdb	
		dc.w SME_8oSdb_B4E-SME_8oSdb, SME_8oSdb_B68-SME_8oSdb	
		dc.w SME_8oSdb_B82-SME_8oSdb, SME_8oSdb_B96-SME_8oSdb	
		dc.w SME_8oSdb_BB0-SME_8oSdb, SME_8oSdb_BCA-SME_8oSdb	
		dc.w SME_8oSdb_BE4-SME_8oSdb, SME_8oSdb_BF8-SME_8oSdb	
		dc.w SME_8oSdb_C12-SME_8oSdb, SME_8oSdb_C26-SME_8oSdb	
		dc.w SME_8oSdb_C34-SME_8oSdb, SME_8oSdb_C48-SME_8oSdb	
		dc.w SME_8oSdb_C56-SME_8oSdb, SME_8oSdb_C64-SME_8oSdb	
		dc.w SME_8oSdb_C72-SME_8oSdb, SME_8oSdb_C80-SME_8oSdb	
		dc.w SME_8oSdb_C8E-SME_8oSdb, SME_8oSdb_CA2-SME_8oSdb	
		dc.w SME_8oSdb_CB6-SME_8oSdb, SME_8oSdb_CC4-SME_8oSdb	
		dc.w SME_8oSdb_CD8-SME_8oSdb, SME_8oSdb_CF2-SME_8oSdb	
		dc.w SME_8oSdb_D0C-SME_8oSdb, SME_8oSdb_D26-SME_8oSdb	
		dc.w SME_8oSdb_D34-SME_8oSdb, SME_8oSdb_D54-SME_8oSdb	
		dc.w SME_8oSdb_D74-SME_8oSdb, SME_8oSdb_D7C-SME_8oSdb	
		dc.w SME_8oSdb_D84-SME_8oSdb, SME_8oSdb_D8C-SME_8oSdb	
		dc.w SME_8oSdb_D94-SME_8oSdb, SME_8oSdb_D9C-SME_8oSdb	
		dc.w SME_8oSdb_DAA-SME_8oSdb, SME_8oSdb_DB8-SME_8oSdb	
		dc.w SME_8oSdb_DD2-SME_8oSdb, SME_8oSdb_DEC-SME_8oSdb	
		dc.w SME_8oSdb_E06-SME_8oSdb, SME_8oSdb_E1A-SME_8oSdb	
		dc.w SME_8oSdb_E2E-SME_8oSdb, SME_8oSdb_E48-SME_8oSdb	
		dc.w SME_8oSdb_E5C-SME_8oSdb, SME_8oSdb_E76-SME_8oSdb	
		dc.w SME_8oSdb_E8A-SME_8oSdb, SME_8oSdb_EA4-SME_8oSdb	
		dc.w SME_8oSdb_EB8-SME_8oSdb, SME_8oSdb_ECC-SME_8oSdb	
		dc.w SME_8oSdb_EE0-SME_8oSdb, SME_8oSdb_EF4-SME_8oSdb	
		dc.w SME_8oSdb_F08-SME_8oSdb, SME_8oSdb_F22-SME_8oSdb	
		dc.w SME_8oSdb_F3C-SME_8oSdb, SME_8oSdb_F56-SME_8oSdb	
		dc.w SME_8oSdb_F70-SME_8oSdb, SME_8oSdb_F7E-SME_8oSdb	
		dc.w SME_8oSdb_F92-SME_8oSdb, SME_8oSdb_FA6-SME_8oSdb	
		dc.w SME_8oSdb_FBA-SME_8oSdb, SME_8oSdb_FCE-SME_8oSdb	
		dc.w SME_8oSdb_FE2-SME_8oSdb, SME_8oSdb_FF6-SME_8oSdb	
		dc.w SME_8oSdb_100A-SME_8oSdb, SME_8oSdb_101E-SME_8oSdb	
		dc.w SME_8oSdb_1032-SME_8oSdb, SME_8oSdb_104C-SME_8oSdb	
		dc.w SME_8oSdb_1060-SME_8oSdb, SME_8oSdb_107A-SME_8oSdb	
		dc.w SME_8oSdb_1094-SME_8oSdb, SME_8oSdb_10AE-SME_8oSdb	
		dc.w SME_8oSdb_10C8-SME_8oSdb, SME_8oSdb_10E2-SME_8oSdb	
		dc.w SME_8oSdb_10F0-SME_8oSdb, SME_8oSdb_110A-SME_8oSdb	
		dc.w SME_8oSdb_1124-SME_8oSdb, SME_8oSdb_113E-SME_8oSdb	
		dc.w SME_8oSdb_1152-SME_8oSdb, SME_8oSdb_116C-SME_8oSdb	
		dc.w SME_8oSdb_1186-SME_8oSdb, SME_8oSdb_119A-SME_8oSdb	
		dc.w SME_8oSdb_11AE-SME_8oSdb, SME_8oSdb_11BC-SME_8oSdb	
		dc.w SME_8oSdb_11D0-SME_8oSdb, SME_8oSdb_11E4-SME_8oSdb	
		dc.w SME_8oSdb_11F2-SME_8oSdb, SME_8oSdb_1200-SME_8oSdb	
		dc.w SME_8oSdb_120E-SME_8oSdb, SME_8oSdb_1222-SME_8oSdb	
		dc.w SME_8oSdb_1236-SME_8oSdb, SME_8oSdb_123E-SME_8oSdb	
		dc.w SME_8oSdb_1252-SME_8oSdb, SME_8oSdb_1260-SME_8oSdb	
		dc.w SME_8oSdb_1274-SME_8oSdb, SME_8oSdb_1288-SME_8oSdb	
		dc.w SME_8oSdb_129C-SME_8oSdb, SME_8oSdb_12B0-SME_8oSdb	
		dc.w SME_8oSdb_12CA-SME_8oSdb, SME_8oSdb_12DE-SME_8oSdb	
		dc.w SME_8oSdb_12F2-SME_8oSdb, SME_8oSdb_1306-SME_8oSdb	
		dc.w SME_8oSdb_131A-SME_8oSdb, SME_8oSdb_132E-SME_8oSdb	
		dc.w SME_8oSdb_1342-SME_8oSdb, SME_8oSdb_134A-SME_8oSdb	
		dc.w SME_8oSdb_1358-SME_8oSdb, SME_8oSdb_1372-SME_8oSdb	
		dc.w SME_8oSdb_138C-SME_8oSdb, SME_8oSdb_13A6-SME_8oSdb	
		dc.w SME_8oSdb_13C0-SME_8oSdb, SME_8oSdb_13D4-SME_8oSdb	
		dc.w SME_8oSdb_13E2-SME_8oSdb, SME_8oSdb_13FC-SME_8oSdb	
		dc.w SME_8oSdb_1410-SME_8oSdb, SME_8oSdb_141E-SME_8oSdb	
		dc.w SME_8oSdb_1438-SME_8oSdb, SME_8oSdb_144C-SME_8oSdb	
		dc.w SME_8oSdb_1460-SME_8oSdb, SME_8oSdb_147A-SME_8oSdb	
		dc.w SME_8oSdb_1488-SME_8oSdb, SME_8oSdb_149C-SME_8oSdb	
		dc.w SME_8oSdb_14AA-SME_8oSdb, SME_8oSdb_14B8-SME_8oSdb	
		dc.w SME_8oSdb_14C6-SME_8oSdb, SME_8oSdb_14E0-SME_8oSdb	
		dc.w SME_8oSdb_14F4-SME_8oSdb, SME_8oSdb_1502-SME_8oSdb	
		dc.w SME_8oSdb_1510-SME_8oSdb, SME_8oSdb_1524-SME_8oSdb	
		dc.w SME_8oSdb_1532-SME_8oSdb, SME_8oSdb_1540-SME_8oSdb	
		dc.w SME_8oSdb_156C-SME_8oSdb, SME_8oSdb_1598-SME_8oSdb	
		dc.w SME_8oSdb_15C4-SME_8oSdb, SME_8oSdb_15F0-SME_8oSdb	
		dc.w SME_8oSdb_1616-SME_8oSdb, SME_8oSdb_1636-SME_8oSdb	
		dc.w SME_8oSdb_165C-SME_8oSdb, SME_8oSdb_167C-SME_8oSdb	
		dc.w SME_8oSdb_169C-SME_8oSdb, SME_8oSdb_16BC-SME_8oSdb	
		dc.w SME_8oSdb_16DC-SME_8oSdb, SME_8oSdb_16FC-SME_8oSdb	
		dc.w SME_8oSdb_1734-SME_8oSdb, SME_8oSdb_1760-SME_8oSdb	
		dc.w SME_8oSdb_1798-SME_8oSdb	
SME_8oSdb_216:	dc.b 0, 0	
SME_8oSdb_218:	dc.b 0, 3	
		dc.b $EC, $D, 0, 0, $FF, $EC	
		dc.b $FC, 6, 0, 8, 0, 4	
		dc.b $FC, $A, 0, $E, $FF, $EC	
SME_8oSdb_22C:	dc.b 0, 3	
		dc.b $FD, $E, 0, 0, $FF, $EC	
		dc.b 5, 1, 0, $C, 0, $C	
		dc.b $ED, $D, 0, $E, $FF, $EC	
SME_8oSdb_240:	dc.b 0, 3	
		dc.b $FE, $A, 0, 0, $FF, $F4	
		dc.b $F6, $C, 0, 9, $FF, $EC	
		dc.b $EE, 8, 0, $D, $FF, $F4	
SME_8oSdb_254:	dc.b 0, 2	
		dc.b $FC, $E, 0, 0, $FF, $EE	
		dc.b $EC, 9, 0, $C, $FF, $F4	
SME_8oSdb_262:	dc.b 0, 3	
		dc.b $FC, $E, 0, 0, $FF, $EE	
		dc.b $FC, 1, 0, $C, 0, $E	
		dc.b $EC, 9, 0, $E, $FF, $F4	
SME_8oSdb_276:	dc.b 0, 3	
		dc.b $FD, $E, 0, 0, $FF, $ED	
		dc.b 5, 1, 0, $C, 0, $D	
		dc.b $ED, 9, 0, $E, $FF, $F4	
SME_8oSdb_28A:	dc.b 0, 3	
		dc.b $FE, $A, 0, 0, $FF, $F4	
		dc.b $F6, $C, 0, 9, $FF, $EC	
		dc.b $EE, 8, 0, $D, $FF, $F4	
SME_8oSdb_29E:	dc.b 0, 2	
		dc.b $FC, $A, 0, 0, $FF, $F4	
		dc.b $EC, $D, 0, 9, $FF, $EC	
SME_8oSdb_2AC:	dc.b 0, 4	
		dc.b $FD, $B, 0, 0, $FF, $FB	
		dc.b $ED, $D, 0, $C, $FF, $EB	
		dc.b $F5, 0, 0, $14, 0, $B	
		dc.b $FD, 5, 0, $15, $FF, $EB	
SME_8oSdb_2C6:	dc.b 0, 4	
		dc.b 2, $E, 0, 0, $FF, $FC	
		dc.b $F2, $D, 0, $C, $FF, $EC	
		dc.b $FA, 4, 0, $14, 0, $C	
		dc.b 2, 4, 0, $16, $FF, $EC	
SME_8oSdb_2E0:	dc.b 0, 4	
		dc.b $FB, $A, 0, 0, $FF, $FD	
		dc.b $EB, 4, 0, 9, $FF, $F5	
		dc.b $F3, $C, 0, $B, $FF, $ED	
		dc.b $FB, 5, 0, $F, $FF, $ED	
SME_8oSdb_2FA:	dc.b 0, 4	
		dc.b $FD, $E, 0, 0, $FF, $FC	
		dc.b $ED, $D, 0, $C, $FF, $EC	
		dc.b $FD, 4, 0, $14, $FF, $EC	
		dc.b 5, 0, 0, $16, $FF, $F4	
SME_8oSdb_314:	dc.b 0, 4	
		dc.b $FE, $B, 0, 0, $FF, $FC	
		dc.b $EE, $D, 0, $C, $FF, $EC	
		dc.b $EE, 1, 0, $14, 0, $C	
		dc.b $FE, 6, 0, $16, $FF, $EC	
SME_8oSdb_32E:	dc.b 0, 4	
		dc.b $FC, $F, 0, 0, $FF, $FB	
		dc.b $EC, 9, 0, $10, $FF, $EB	
		dc.b $F4, 8, 0, $16, 0, 3	
		dc.b $FC, 5, 0, $19, $FF, $EB	
SME_8oSdb_348:	dc.b 0, 4	
		dc.b $FF, $A, 0, 0, $FF, $FA	
		dc.b $EF, $D, 0, 9, $FF, $EA	
		dc.b $FF, 4, 0, $11, $FF, $EA	
		dc.b 7, 0, 0, $13, $FF, $F2	
SME_8oSdb_362:	dc.b 0, 4	
		dc.b $FE, $A, 0, 0, $FF, $FC	
		dc.b $EE, 8, 0, 9, $FF, $EC	
		dc.b $F6, $C, 0, $C, $FF, $EC	
		dc.b $FE, 5, 0, $10, $FF, $EC	
SME_8oSdb_37C:	dc.b 0, 3	
		dc.b $ED, 9, 0, 0, $FF, $FC	
		dc.b $FD, $A, 0, 6, $FF, $FC	
		dc.b $F5, 7, 0, $F, $FF, $EC	
SME_8oSdb_390:	dc.b 0, 3	
		dc.b $F5, $B, 0, 0, $FF, $FC	
		dc.b $ED, 4, 0, $C, 0, 4	
		dc.b $F5, 7, 0, $E, $FF, $EC	
SME_8oSdb_3A4:	dc.b 0, 3	
		dc.b $F5, $A, 0, 0, $FF, $FC	
		dc.b $F5, 6, 0, 9, $FF, $EC	
		dc.b $D, 0, 0, $F, $FF, $F4	
SME_8oSdb_3B8:	dc.b 0, 2	
		dc.b $F3, $B, 0, 0, $FF, $FC	
		dc.b $F3, 7, 0, $C, $FF, $EC	
SME_8oSdb_3C6:	dc.b 0, 3	
		dc.b $EB, 4, 0, 0, $FF, $FC	
		dc.b $F3, $B, 0, 2, $FF, $FC	
		dc.b $F3, 7, 0, $E, $FF, $EC	
SME_8oSdb_3DA:	dc.b 0, 4	
		dc.b $EC, 4, 0, 0, 0, 4	
		dc.b $F4, $B, 0, 2, $FF, $FC	
		dc.b $F4, 6, 0, $E, $FF, $EC	
		dc.b $C, 0, 0, $14, $FF, $F4	
SME_8oSdb_3F4:	dc.b 0, 3	
		dc.b $F5, $A, 0, 0, $FF, $FC	
		dc.b $F5, 6, 0, 9, $FF, $EC	
		dc.b $D, 0, 0, $F, $FF, $F4	
SME_8oSdb_408:	dc.b 0, 2	
		dc.b $F5, $A, 0, 0, $FF, $FC	
		dc.b $F5, 7, 0, 9, $FF, $EC	
SME_8oSdb_416:	dc.b 0, 4	
		dc.b $EF, $E, 0, 0, $FF, $FE	
		dc.b $EF, 6, 0, $C, $FF, $EE	
		dc.b 7, $C, 0, $12, $FF, $EE	
		dc.b $F, 8, 0, $16, $FF, $EE	
SME_8oSdb_430:	dc.b 0, 4	
		dc.b $E7, $B, 0, 0, 0, 0	
		dc.b $E7, 1, 0, $C, $FF, $F8	
		dc.b $F7, 5, 0, $E, $FF, $F0	
		dc.b 7, 9, 0, $12, $FF, $F0	
SME_8oSdb_44A:	dc.b 0, 4	
		dc.b $EF, $A, 0, 0, $FF, $FD	
		dc.b $F7, 5, 0, 9, $FF, $ED	
		dc.b 7, $C, 0, $D, $FF, $ED	
		dc.b $F, 4, 0, $11, $FF, $F5	
SME_8oSdb_464:	dc.b 0, 4	
		dc.b $E7, $B, 0, 0, 0, 0	
		dc.b $F7, 5, 0, $C, $FF, $F0	
		dc.b 7, $C, 0, $10, $FF, $F0	
		dc.b $F, 8, 0, $14, $FF, $F0	
SME_8oSdb_47E:	dc.b 0, 4	
		dc.b $EE, $E, 0, 0, $FF, $FF	
		dc.b $EE, 6, 0, $C, $FF, $EF	
		dc.b 6, $D, 0, $12, $FF, $EF	
		dc.b 6, 0, 0, $1A, 0, $F	
SME_8oSdb_498:	dc.b 0, 4	
		dc.b $E8, $F, 0, 0, $FF, $FF	
		dc.b $E8, 2, 0, $10, $FF, $F7	
		dc.b 0, 4, 0, $13, $FF, $EF	
		dc.b 8, $D, 0, $15, $FF, $EF	
SME_8oSdb_4B2:	dc.b 0, 4	
		dc.b $EE, $A, 0, 0, $FF, $FE	
		dc.b $F6, 5, 0, 9, $FF, $EE	
		dc.b 6, $C, 0, $D, $FF, $EE	
		dc.b $E, 8, 0, $11, $FF, $EE	
SME_8oSdb_4CC:	dc.b 0, 4	
		dc.b $EE, $A, 0, 0, $FF, $FE	
		dc.b $F6, 5, 0, 9, $FF, $EE	
		dc.b 6, $C, 0, $D, $FF, $EE	
		dc.b $E, 8, 0, $11, $FF, $EE	
SME_8oSdb_4E6:	dc.b 0, 2	
		dc.b $EC, 8, 0, 0, $FF, $F8	
		dc.b $F4, $F, 0, 3, $FF, $F0	
SME_8oSdb_4F4:	dc.b 0, 2	
		dc.b $EC, 8, 0, 0, $FF, $F8	
		dc.b $F4, $F, 0, 3, $FF, $F0	
SME_8oSdb_502:	dc.b 0, 2	
		dc.b $EC, 8, 0, 0, $FF, $F8	
		dc.b $F4, $F, 0, 3, $FF, $F0	
SME_8oSdb_510:	dc.b 0, 2	
		dc.b $EC, 8, 0, 0, $FF, $F8	
		dc.b $F4, $F, 0, 3, $FF, $F0	
SME_8oSdb_51E:	dc.b 0, 4	
		dc.b $EF, $E, 0, 0, $FF, $ED	
		dc.b $FF, 0, 0, $C, 0, $D	
		dc.b 7, $C, 0, $D, $FF, $F5	
		dc.b $F, 8, 0, $11, $FF, $F5	
SME_8oSdb_538:	dc.b 0, 2	
		dc.b $EE, 8, 0, 0, $FF, $F0	
		dc.b $F6, $F, 0, 3, $FF, $F0	
SME_8oSdb_546:	dc.b 0, 4	
		dc.b $EF, $E, 0, 0, $FF, $ED	
		dc.b $FF, 0, 0, $C, 0, $D	
		dc.b 7, $C, 0, $D, $FF, $F5	
		dc.b $F, 8, 0, $11, $FF, $F5	
SME_8oSdb_560:	dc.b 0, 2	
		dc.b $EE, 8, 0, 0, $FF, $F0	
		dc.b $F6, $F, 0, 3, $FF, $F0	
SME_8oSdb_56E:	dc.b 0, 2	
		dc.b $F0, 2, 0, 0, $FF, $EC	
		dc.b $F0, $F, 0, 3, $FF, $F4	
SME_8oSdb_57C:	dc.b 0, 2	
		dc.b $F0, 2, 0, 0, $FF, $EC	
		dc.b $F0, $F, 0, 3, $FF, $F4	
SME_8oSdb_58A:	dc.b 0, 2	
		dc.b $F0, 2, 0, 0, $FF, $EC	
		dc.b $F0, $F, 0, 3, $FF, $F4	
SME_8oSdb_598:	dc.b 0, 2	
		dc.b $F0, 2, 0, 0, $FF, $EC	
		dc.b $F0, $F, 0, 3, $FF, $F4	
SME_8oSdb_5A6:	dc.b 0, 3	
		dc.b $EA, 4, 0, 0, $FF, $FE	
		dc.b $F2, $B, 0, 2, $FF, $EE	
		dc.b $F2, 6, 0, $E, 0, 6	
SME_8oSdb_5BA:	dc.b 0, 2	
		dc.b $F7, 2, 0, 0, $FF, $ED	
		dc.b $EF, $F, 0, 3, $FF, $F5	
SME_8oSdb_5C8:	dc.b 0, 3	
		dc.b $EA, 4, 0, 0, $FF, $FE	
		dc.b $F2, $B, 0, 2, $FF, $EE	
		dc.b $F2, 6, 0, $E, 0, 6	
SME_8oSdb_5DC:	dc.b 0, 2	
		dc.b $F7, 2, 0, 0, $FF, $ED	
		dc.b $EF, $F, 0, 3, $FF, $F5	
SME_8oSdb_5EA:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $F2	
		dc.b $F4, $E, 0, 3, $FF, $F2	
		dc.b $C, 8, 0, $F, $FF, $F2	
SME_8oSdb_5FE:	dc.b 0, 4	
		dc.b $EA, 9, 0, 0, $FF, $F3	
		dc.b $FA, $C, 0, 6, $FF, $F3	
		dc.b 2, 8, 0, $A, $FF, $F3	
		dc.b $A, 4, 0, $D, $FF, $FB	
SME_8oSdb_618:	dc.b 0, 4	
		dc.b $F0, 4, 0, 0, $FF, $F9	
		dc.b $F8, 8, 0, 2, $FF, $F1	
		dc.b 0, $C, 0, 5, $FF, $F1	
		dc.b 8, 4, 0, 9, $FF, $F9	
SME_8oSdb_632:	dc.b 0, 4	
		dc.b $EF, 8, 0, 0, $FF, $F9	
		dc.b $F7, $C, 0, 3, $FF, $F1	
		dc.b $FF, 8, 0, 7, $FF, $F1	
		dc.b 7, $C, 0, $A, $FF, $F1	
SME_8oSdb_64C:	dc.b 0, 4	
		dc.b $EF, 4, 0, 0, $FF, $F4	
		dc.b $F7, 8, 0, 2, $FF, $F4	
		dc.b $FF, $C, 0, 5, $FF, $F4	
		dc.b 7, 8, 0, 9, $FF, $F4	
SME_8oSdb_666:	dc.b 0, 3	
		dc.b $EA, 4, 0, 0, $FF, $F9	
		dc.b $F2, $D, 0, 2, $FF, $F1	
		dc.b 2, 9, 0, $A, $FF, $F1	
SME_8oSdb_67A:	dc.b 0, 2	
		dc.b $EC, $F, 0, 0, $FF, $F0	
		dc.b $C, 8, 0, $10, $FF, $F0	
SME_8oSdb_688:	dc.b 0, 2	
		dc.b $EE, $B, 0, 0, $FF, $F4	
		dc.b $E, 0, 0, $C, $FF, $FC	
SME_8oSdb_696:	dc.b 0, 3	
		dc.b $EE, 4, 0, 0, $FF, $F0	
		dc.b $F6, $D, 0, 2, $FF, $F0	
		dc.b 6, 8, 0, $A, $FF, $F0	
SME_8oSdb_6AA:	dc.b 0, 4	
		dc.b $F0, 8, 0, 0, $FF, $F5	
		dc.b $F8, $C, 0, 3, $FF, $ED	
		dc.b 0, 8, 0, 7, $FF, $F5	
		dc.b 8, 4, 0, $A, $FF, $F5	
SME_8oSdb_6C4:	dc.b 0, 2	
		dc.b $EC, $A, 0, 0, $FF, $F4	
		dc.b 4, 5, 0, 9, $FF, $FC	
SME_8oSdb_6D2:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F4	
		dc.b $C, 8, 0, $C, $FF, $F4	
SME_8oSdb_6E0:	dc.b 0, 3	
		dc.b $E6, 0, 0, 0, $FF, $FA	
		dc.b $EE, $B, 0, 1, $FF, $F2	
		dc.b $E, 8, 0, $D, $FF, $F2	
SME_8oSdb_6F4:	dc.b 0, 1	
		dc.b $F0, $B, 0, 0, $FF, $F3	
SME_8oSdb_6FC:	dc.b 0, 3	
		dc.b $F1, 8, 0, 0, $FF, $EF	
		dc.b $F9, $D, 0, 3, $FF, $EF	
		dc.b 9, 4, 0, $B, $FF, $F7	
SME_8oSdb_710:	dc.b 0, 4	
		dc.b $F2, 8, 0, 0, $FF, $F8	
		dc.b $FA, $C, 0, 3, $FF, $F0	
		dc.b 2, 8, 0, 7, $FF, $F0	
		dc.b $A, 4, 0, $A, $FF, $F8	
SME_8oSdb_72A:	dc.b 0, 4	
		dc.b $EF, 9, 0, 0, $FF, $F8	
		dc.b $FF, $C, 0, 6, $FF, $F0	
		dc.b 7, 8, 0, $A, $FF, $F0	
		dc.b $F, 4, 0, $D, $FF, $F8	
SME_8oSdb_744:	dc.b 0, 4	
		dc.b $EC, 9, 0, 0, $FF, $F8	
		dc.b $FC, 8, 0, 6, $FF, $F0	
		dc.b 4, $C, 0, 9, $FF, $F0	
		dc.b $C, 8, 0, $D, $FF, $F0	
SME_8oSdb_75E:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F5	
		dc.b $C, 8, 0, $C, $FF, $F5	
SME_8oSdb_76C:	dc.b 0, 1	
		dc.b $F0, $B, 0, 0, $FF, $F4	
SME_8oSdb_774:	dc.b 0, 2	
		dc.b $F6, $D, 0, 0, $FF, $F2	
		dc.b 6, 8, 0, 8, $FF, $F2	
SME_8oSdb_782:	dc.b 0, 1	
		dc.b $F3, $B, 0, 0, $FF, $F3	
SME_8oSdb_78A:	dc.b 0, 2	
		dc.b $EB, 4, 0, 0, $FF, $FB	
		dc.b $F3, $B, 0, 2, $FF, $F3	
SME_8oSdb_798:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $F0	
		dc.b $F4, $D, 0, 3, $FF, $F0	
		dc.b 4, 9, 0, $B, $FF, $F8	
SME_8oSdb_7AC:	dc.b 0, 4	
		dc.b $E8, $A, 0, 0, $FF, $FC	
		dc.b $F0, 5, 0, 9, $FF, $EC	
		dc.b 0, $C, 0, $D, $FF, $EC	
		dc.b 8, 8, 0, $11, $FF, $F4	
SME_8oSdb_7C6:	dc.b 0, 4	
		dc.b $EA, 8, 0, 0, $FF, $FA	
		dc.b $F2, $D, 0, 3, $FF, $F2	
		dc.b 2, 8, 0, $B, $FF, $F2	
		dc.b $A, 5, 0, $E, $FF, $F2	
SME_8oSdb_7E0:	dc.b 0, 3	
		dc.b $EB, 9, 0, 0, $FF, $F8	
		dc.b $FB, $C, 0, 6, $FF, $F0	
		dc.b 3, 8, 0, $A, $FF, $F8	
SME_8oSdb_7F4:	dc.b 0, 3	
		dc.b $F1, 8, 0, 0, $FF, $F3	
		dc.b $F9, $D, 0, 3, $FF, $F3	
		dc.b 9, 8, 0, $B, $FF, $F3	
SME_8oSdb_808:	dc.b 0, 3	
		dc.b $EE, 4, 0, 0, $FF, $FB	
		dc.b $F6, $D, 0, 2, $FF, $F3	
		dc.b 6, 4, 0, $A, $FF, $FB	
SME_8oSdb_81C:	dc.b 0, 1	
		dc.b $F2, $F, 0, 0, $FF, $EE	
SME_8oSdb_824:	dc.b 0, 4	
		dc.b $EA, 0, 0, 0, 0, 2	
		dc.b $F2, $D, 0, 1, $FF, $F2	
		dc.b $FA, 0, 0, 9, $FF, $EA	
		dc.b 2, $D, 0, $A, $FF, $EA	
SME_8oSdb_83E:	dc.b 0, 4	
		dc.b $EC, 4, 0, 0, $FF, $FB	
		dc.b $F4, 8, 0, 2, $FF, $F3	
		dc.b $FC, $A, 0, 5, $FF, $EB	
		dc.b $FC, 5, 0, $E, 0, 3	
SME_8oSdb_858:	dc.b 0, 2	
		dc.b $ED, 4, 0, 0, $FF, $FF	
		dc.b $F5, $E, 0, 2, $FF, $EF	
SME_8oSdb_866:	dc.b 0, 3	
		dc.b $EF, 4, 0, 0, $FF, $FA	
		dc.b $F7, $D, 0, 2, $FF, $F2	
		dc.b 7, 4, 0, $A, $FF, $FA	
SME_8oSdb_87A:	dc.b 0, 3	
		dc.b $ED, 0, 0, 0, 0, 0	
		dc.b $F5, $A, 0, 1, $FF, $F8	
		dc.b $FD, 4, 0, $A, $FF, $E8	
SME_8oSdb_88E:	dc.b 0, 2	
		dc.b $F0, $F, 0, 0, $FF, $EF	
		dc.b $F8, 0, 0, $10, 0, $F	
SME_8oSdb_89C:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $F0	
		dc.b $F4, $D, 0, 3, $FF, $F0	
		dc.b 4, 8, 0, $B, $FF, $F0	
		dc.b $C, 8, 0, $E, $FF, $F8	
SME_8oSdb_8B6:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $EF	
		dc.b $F4, $D, 0, 3, $FF, $EF	
		dc.b 4, 9, 0, $B, $FF, $F7	
SME_8oSdb_8CA:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F0	
		dc.b $C, $C, 0, $C, $FF, $F0	
SME_8oSdb_8D8:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F3	
		dc.b $C, 8, 0, $C, $FF, $F3	
SME_8oSdb_8E6:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $F7	
		dc.b $F4, $C, 0, 3, $FF, $EF	
		dc.b $FC, 8, 0, 7, $FF, $F7	
		dc.b 4, $D, 0, $A, $FF, $EF	
SME_8oSdb_900:	dc.b 0, 3	
		dc.b $EC, $A, 0, 0, $FF, $F6	
		dc.b 4, $C, 0, 9, $FF, $EE	
		dc.b $C, 8, 0, $D, $FF, $F6	
SME_8oSdb_914:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F3	
		dc.b $C, 8, 0, $C, $FF, $F3	
SME_8oSdb_922:	dc.b 0, 3	
		dc.b $F5, $A, 0, 0, $FF, $EC	
		dc.b $F5, 9, 0, 9, 0, 4	
		dc.b 5, 0, 0, $F, 0, $14	
SME_8oSdb_936:	dc.b 0, 1	
		dc.b $F4, $E, 0, 0, $FF, $FC	
SME_8oSdb_93E:	dc.b 0, 2	
		dc.b $F4, $E, 0, 0, $FF, $E2	
		dc.b $F4, 2, 0, $C, 0, 2	
SME_8oSdb_94C:	dc.b 0, 3	
		dc.b $F5, 5, 0, 0, $FF, $E5	
		dc.b $F5, $E, 0, 4, $FF, $F5	
		dc.b 5, 0, 0, $10, $FF, $E5	
SME_8oSdb_960:	dc.b 0, 1	
		dc.b $F4, $E, 0, 0, $FF, $F6	
SME_8oSdb_968:	dc.b 0, 2	
		dc.b $FC, 0, 0, 0, $FF, $DA	
		dc.b $F4, $E, 0, 1, $FF, $E2	
SME_8oSdb_976:	dc.b 0, 2	
		dc.b $F0, $A, 0, 0, $FF, $EC	
		dc.b $F8, 6, 0, 9, 0, 4	
SME_8oSdb_984:	dc.b 0, 3	
		dc.b $E8, $A, 0, 0, $FF, $F5	
		dc.b 0, 5, 0, 9, $FF, $F5	
		dc.b $10, 8, 0, $D, $FF, $F5	
SME_8oSdb_998:	dc.b 0, 1	
		dc.b $F1, $B, 0, 0, $FF, $F3	
SME_8oSdb_9A0:	dc.b 0, 2	
		dc.b $EB, $B, 0, 0, $FF, $F4	
		dc.b $B, 8, 0, $C, $FF, $F4	
SME_8oSdb_9AE:	dc.b 0, 3	
		dc.b $E8, 8, 0, 0, $FF, $F4	
		dc.b $F0, 4, 0, 3, $FF, $F4	
		dc.b $F8, $B, 0, 5, $FF, $F4	
SME_8oSdb_9C2:	dc.b 0, 1	
		dc.b $F0, $B, 0, 0, $FF, $F2	
SME_8oSdb_9CA:	dc.b 0, 2	
		dc.b $E8, 0, 0, 0, $FF, $FA	
		dc.b $F0, $B, 0, 1, $FF, $F2	
SME_8oSdb_9D8:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F0	
		dc.b $C, 8, 0, $C, $FF, $F8	
SME_8oSdb_9E6:	dc.b 0, 4	
		dc.b $EC, 4, 0, 0, $FF, $F0	
		dc.b $F4, $C, 0, 2, $FF, $F0	
		dc.b $FC, 9, 0, 6, $FF, $F0	
		dc.b $C, 4, 0, $C, $FF, $F8	
SME_8oSdb_A00:	dc.b 0, 3	
		dc.b $EC, 0, 0, 0, $FF, $FB	
		dc.b $F4, $D, 0, 1, $FF, $EB	
		dc.b 4, 8, 0, 9, $FF, $F3	
SME_8oSdb_A14:	dc.b 0, 3	
		dc.b $F0, 0, 0, 0, 0, 0	
		dc.b $F8, $D, 0, 1, $FF, $F0	
		dc.b 8, 8, 0, 9, $FF, $F0	
SME_8oSdb_A28:	dc.b 0, 2	
		dc.b $F8, 6, 0, 0, $FF, $EC	
		dc.b $F0, $B, 0, 6, $FF, $FC	
SME_8oSdb_A36:	dc.b 0, 3	
		dc.b $F0, 0, 0, 0, $FF, $FC	
		dc.b $F8, $A, 0, 1, $FF, $EC	
		dc.b $F8, 5, 0, $A, 0, 4	
SME_8oSdb_A4A:	dc.b 0, 3	
		dc.b $F0, 4, 0, 0, $FF, $F8	
		dc.b $F8, 9, 0, 2, $FF, $F0	
		dc.b 8, $C, 0, 8, $FF, $F0	
SME_8oSdb_A5E:	dc.b 0, 3	
		dc.b $F0, 9, 0, 0, $FF, $F8	
		dc.b 0, $C, 0, 6, $FF, $F0	
		dc.b 8, 4, 0, $A, 0, 0	
SME_8oSdb_A72:	dc.b 0, 2	
		dc.b $F0, 0, 0, 0, 0, 0	
		dc.b $F8, $E, 0, 1, $FF, $F0	
SME_8oSdb_A80:	dc.b 0, 4	
		dc.b $F0, 4, 0, 0, $FF, $F4	
		dc.b $F8, 0, 0, 2, $FF, $EC	
		dc.b 8, 0, 0, 3, $FF, $EC	
		dc.b $F8, $E, 0, 4, $FF, $F4	
SME_8oSdb_A9A:	dc.b 0, 3	
		dc.b $F0, 4, 0, 0, $FF, $FC	
		dc.b $F8, $E, 0, 2, $FF, $EC	
		dc.b $F8, 2, 0, $E, 0, $C	
SME_8oSdb_AAE:	dc.b 0, 3	
		dc.b $F0, 4, 0, 0, $FF, $F8	
		dc.b $F8, $D, 0, 2, $FF, $F0	
		dc.b 8, 8, 0, $A, $FF, $F8	
SME_8oSdb_AC2:	dc.b 0, 2	
		dc.b $F0, 0, 0, 0, $FF, $F4	
		dc.b $F8, $A, 0, 1, $FF, $F4	
SME_8oSdb_AD0:	dc.b 0, 3	
		dc.b $F0, 8, 0, 0, $FF, $F0	
		dc.b $F8, $D, 0, 3, $FF, $F0	
		dc.b 8, 8, 0, $B, $FF, $F0	
SME_8oSdb_AE4:	dc.b 0, 1	
		dc.b $F4, $E, 0, 0, $FF, $F0	
SME_8oSdb_AEC:	dc.b 0, 4	
		dc.b $E7, $C, 0, 0, $FF, $EE	
		dc.b $EF, 8, 0, 4, $FF, $EE	
		dc.b $F7, $D, 0, 7, $FF, $EE	
		dc.b 7, $D, 0, $F, $FF, $F6	
SME_8oSdb_B06:	dc.b 0, 4	
		dc.b $E6, 4, 0, 0, $FF, $E9	
		dc.b $EE, $E, 0, 2, $FF, $E1	
		dc.b $F6, 6, 0, $E, 0, 1	
		dc.b 6, 8, 0, $14, $FF, $E9	
SME_8oSdb_B20:	dc.b 0, 3	
		dc.b $F1, 8, 0, 0, $FF, $E6	
		dc.b $F9, $E, 0, 3, $FF, $E6	
		dc.b $E9, 7, 0, $F, 0, 6	
SME_8oSdb_B34:	dc.b 0, 4	
		dc.b $EE, 8, 0, 0, $FF, $F5	
		dc.b $F6, 4, 0, 3, $FF, $FD	
		dc.b 6, 1, 0, 5, $FF, $E5	
		dc.b $FE, $F, 0, 7, $FF, $ED	
SME_8oSdb_B4E:	dc.b 0, 4	
		dc.b $12, $C, $18, 0, $FF, $F1	
		dc.b $A, 8, $18, 4, $FF, $F9	
		dc.b $FA, $D, $18, 7, $FF, $F1	
		dc.b $EA, $D, $18, $F, $FF, $E9	
SME_8oSdb_B68:	dc.b 0, 4	
		dc.b $13, 4, $18, 0, 0, 4	
		dc.b $FB, $E, $18, 2, $FF, $FC	
		dc.b $F3, 6, $18, $E, $FF, $EC	
		dc.b $F3, 8, $18, $14, $FF, $FC	
SME_8oSdb_B82:	dc.b 0, 3	
		dc.b 7, 8, $18, 0, 0, 1	
		dc.b $EF, $E, $18, 3, $FF, $F9	
		dc.b $F7, 7, $18, $F, $FF, $E9	
SME_8oSdb_B96:	dc.b 0, 4	
		dc.b $C, 8, $18, 0, $FF, $F3	
		dc.b 4, 4, $18, 3, $FF, $F3	
		dc.b $EC, 1, $18, 5, 0, $13	
		dc.b $E4, $F, $18, 7, $FF, $F3	
SME_8oSdb_BB0:	dc.b 0, 4	
		dc.b $E8, $E, 0, 0, $FF, $EF	
		dc.b $E8, 0, 0, $C, 0, $F	
		dc.b 0, 4, 0, $D, $FF, $F7	
		dc.b 8, 9, 0, $F, $FF, $EF	
SME_8oSdb_BCA:	dc.b 0, 4	
		dc.b $E8, $C, 0, 0, $FF, $EE	
		dc.b $F0, $D, 0, 4, $FF, $EE	
		dc.b 0, 5, 0, $C, $FF, $F6	
		dc.b $10, 8, 0, $10, $FF, $EE	
SME_8oSdb_BE4:	dc.b 0, 3	
		dc.b $E8, 8, 0, 0, $FF, $F4	
		dc.b $F0, $C, 0, 3, $FF, $EC	
		dc.b $F8, $B, 0, 7, $FF, $F4	
SME_8oSdb_BF8:	dc.b 0, 4	
		dc.b $E8, 0, 0, 0, $FF, $EA	
		dc.b $E8, $D, 0, 1, $FF, $F2	
		dc.b $F8, 8, 0, 9, $FF, $F2	
		dc.b 0, 6, 0, $C, $FF, $FA	
SME_8oSdb_C12:	dc.b 0, 3	
		dc.b $E8, $E, 0, 0, $FF, $ED	
		dc.b 0, 9, 0, $C, $FF, $F5	
		dc.b $10, 4, 0, $12, $FF, $F5	
SME_8oSdb_C26:	dc.b 0, 2	
		dc.b $E8, $E, 0, 0, $FF, $EF	
		dc.b 0, 6, 0, $C, $FF, $F7	
SME_8oSdb_C34:	dc.b 0, 3	
		dc.b $F8, 8, 0, 0, $FF, $FC	
		dc.b 0, $D, 0, 3, $FF, $F4	
		dc.b $10, 8, 0, $B, $FF, $F4	
SME_8oSdb_C48:	dc.b 0, 2	
		dc.b $F8, $E, 0, 0, $FF, $F4	
		dc.b $10, 8, 0, $C, $FF, $F4	
SME_8oSdb_C56:	dc.b 0, 2	
		dc.b $F8, $E, 0, 0, $FF, $F4	
		dc.b $10, 8, 0, $C, $FF, $F4	
SME_8oSdb_C64:	dc.b 0, 2	
		dc.b $F8, $E, 0, 0, $FF, $F4	
		dc.b $10, 8, 0, $C, $FF, $F4	
SME_8oSdb_C72:	dc.b 0, 2	
		dc.b $F8, $E, 0, 0, $FF, $F4	
		dc.b $10, 8, 0, $C, $FF, $F4	
SME_8oSdb_C80:	dc.b 0, 2	
		dc.b $F8, $E, 0, 0, $FF, $F4	
		dc.b $10, 8, 0, $C, $FF, $F4	
SME_8oSdb_C8E:	dc.b 0, 3	
		dc.b $F0, $E, 0, 0, $FF, $EC	
		dc.b $F0, 2, 0, $C, 0, $C	
		dc.b 8, 8, 0, $F, $FF, $F4	
SME_8oSdb_CA2:	dc.b 0, 3	
		dc.b $EF, $E, 0, 0, $FF, $EC	
		dc.b $EF, 2, 0, $C, 0, $C	
		dc.b 7, 8, 0, $F, $FF, $F4	
SME_8oSdb_CB6:	dc.b 0, 2	
		dc.b $E8, $B, 0, 0, $FF, $F1	
		dc.b 8, 5, 0, $C, $FF, $F9	
SME_8oSdb_CC4:	dc.b 0, 3	
		dc.b $F1, $C, 0, 0, $FF, $E9	
		dc.b $F9, 5, 0, 4, $FF, $E9	
		dc.b $F9, $A, 0, 8, $FF, $F9	
SME_8oSdb_CD8:	dc.b 0, 4	
		dc.b $E8, 9, 0, 0, $FF, $F0	
		dc.b $F8, $C, 0, 6, $FF, $F0	
		dc.b 0, 8, 0, $A, $FF, $F0	
		dc.b 8, 9, 0, $D, $FF, $F8	
SME_8oSdb_CF2:	dc.b 0, 4	
		dc.b $E6, 9, 0, 0, $FF, $F0	
		dc.b $F6, $C, 0, 6, $FF, $F0	
		dc.b $FE, 8, 0, $A, $FF, $F0	
		dc.b 6, $D, 0, $D, $FF, $F8	
SME_8oSdb_D0C:	dc.b 0, 4	
		dc.b $E8, 8, 0, 0, $FF, $F0	
		dc.b $F0, $E, 0, 3, $FF, $F0	
		dc.b 0, 0, 0, $F, 0, $10	
		dc.b 8, 4, 0, $10, 0, 0	
SME_8oSdb_D26:	dc.b 0, 2	
		dc.b $E8, 8, 0, 0, $FF, $F0	
		dc.b $F0, $E, 0, 3, $FF, $F0	
SME_8oSdb_D34:	dc.b 0, 5	
		dc.b $E8, $D, 0, 0, $FF, $F2	
		dc.b $E8, 4, 0, 8, $FF, $F7	
		dc.b $F0, 9, 0, $A, $FF, $F7	
		dc.b 0, 5, 0, $10, $FF, $F7	
		dc.b $10, 8, 0, $14, $FF, $F7	
SME_8oSdb_D54:	dc.b 0, 5	
		dc.b $E8, 8, 0, 0, $FF, $F6	
		dc.b $E8, 4, 0, 3, $FF, $F7	
		dc.b $F0, 9, 0, 5, $FF, $F7	
		dc.b 0, 5, 0, $B, $FF, $F7	
		dc.b $10, 8, 0, $F, $FF, $F7	
SME_8oSdb_D74:	dc.b 0, 1	
		dc.b $F0, $F, 0, 0, $FF, $F0	
SME_8oSdb_D7C:	dc.b 0, 1	
		dc.b $F0, $F, 0, 0, $FF, $F0	
SME_8oSdb_D84:	dc.b 0, 1	
		dc.b $F0, $F, 0, 0, $FF, $F0	
SME_8oSdb_D8C:	dc.b 0, 1	
		dc.b $F0, $F, 0, 0, $FF, $F0	
SME_8oSdb_D94:	dc.b 0, 1	
		dc.b $F0, $F, 0, 0, $FF, $F0	
SME_8oSdb_D9C:	dc.b 0, 2	
		dc.b $EC, 0, 0, 0, 0, 0	
		dc.b $F4, $B, 0, 1, $FF, $F8	
SME_8oSdb_DAA:	dc.b 0, 2	
		dc.b $F4, 0, 0, 0, 0, 3	
		dc.b $FC, $E, 0, 1, $FF, $F3	
SME_8oSdb_DB8:	dc.b 0, 4	
		dc.b $EB, $A, 0, 0, $FF, $F5	
		dc.b $F3, 1, 0, 9, $FF, $ED	
		dc.b 3, $C, 0, $B, $FF, $F5	
		dc.b $B, 8, 0, $F, $FF, $FD	
SME_8oSdb_DD2:	dc.b 0, 4	
		dc.b $EB, $A, 0, 0, $FF, $F6	
		dc.b $F3, 1, 0, 9, $FF, $EE	
		dc.b 3, $C, 0, $B, $FF, $F6	
		dc.b $B, 8, 0, $F, $FF, $FE	
SME_8oSdb_DEC:	dc.b 0, 4	
		dc.b $EA, 8, 0, 0, $FF, $F9	
		dc.b $F2, $D, 0, 3, $FF, $F1	
		dc.b 2, 8, 0, $B, $FF, $F9	
		dc.b $A, 4, 0, $E, 0, 1	
SME_8oSdb_E06:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $EF	
		dc.b $F4, $E, 0, 3, $FF, $EF	
		dc.b $C, 8, 0, $F, $FF, $F7	
SME_8oSdb_E1A:	dc.b 0, 3	
		dc.b $EC, $D, 0, 0, $FF, $EB	
		dc.b $EC, 0, 0, 8, 0, $B	
		dc.b $FC, $A, 0, 9, $FF, $EB	
SME_8oSdb_E2E:	dc.b 0, 4	
		dc.b $E5, 0, 0, 0, $FF, $F4	
		dc.b $ED, $E, 0, 1, $FF, $EC	
		dc.b $ED, 0, 0, $D, $FF, $E4	
		dc.b 5, 9, 0, $E, $FF, $EC	
SME_8oSdb_E48:	dc.b 0, 3	
		dc.b $E4, 0, 0, 0, $FF, $FB	
		dc.b $EC, $F, 0, 1, $FF, $EB	
		dc.b $C, 8, 0, $11, $FF, $EB	
SME_8oSdb_E5C:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $F2	
		dc.b $F4, $E, 0, 3, $FF, $E2	
		dc.b $F4, 1, 0, $F, 0, 2	
		dc.b $C, 8, 0, $11, $FF, $EA	
SME_8oSdb_E76:	dc.b 0, 3	
		dc.b $EC, $F, 0, 0, $FF, $EB	
		dc.b 4, 0, 0, $10, $FF, $E3	
		dc.b $C, 8, 0, $11, $FF, $EB	
SME_8oSdb_E8A:	dc.b 0, 4	
		dc.b $ED, 8, 0, 0, $FF, $F2	
		dc.b $F5, $E, 0, 3, $FF, $E2	
		dc.b $F5, 1, 0, $F, 0, 2	
		dc.b $D, 8, 0, $11, $FF, $EA	
SME_8oSdb_EA4:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $F8	
		dc.b $F4, $F, 0, 3, $FF, $F0	
		dc.b $F4, 0, 0, $13, 0, $10	
SME_8oSdb_EB8:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $F8	
		dc.b $F4, $F, 0, 3, $FF, $F0	
		dc.b $F4, 0, 0, $13, 0, $10	
SME_8oSdb_ECC:	dc.b 0, 3	
		dc.b $EC, 4, 0, 0, $FF, $F8	
		dc.b $F4, $F, 0, 2, $FF, $F0	
		dc.b $F4, 0, 0, $12, 0, $10	
SME_8oSdb_EE0:	dc.b 0, 3	
		dc.b $F4, $E, 0, 0, $FF, $E6	
		dc.b $F4, 0, 0, $C, 0, 6	
		dc.b $FC, 9, 0, $D, 0, 6	
SME_8oSdb_EF4:	dc.b 0, 3	
		dc.b $F4, $E, 0, 0, $FF, $E6	
		dc.b $F4, 0, 0, $C, 0, 6	
		dc.b $FC, 9, 0, $D, 0, 6	
SME_8oSdb_F08:	dc.b 0, 4	
		dc.b $EC, $F, 0, 0, $FF, $EC	
		dc.b 4, 0, 0, $10, 0, $C	
		dc.b $C, 4, 0, $11, $FF, $EC	
		dc.b $C, 4, 0, $13, 0, 4	
SME_8oSdb_F22:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $D, 0, 3, $FF, $EE	
		dc.b 4, 4, 0, $B, $FF, $F6	
		dc.b $C, 8, 0, $D, $FF, $F6	
SME_8oSdb_F3C:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $D, 0, 3, $FF, $EE	
		dc.b 4, 4, 0, $B, $FF, $F6	
		dc.b $C, 8, 0, $D, $FF, $F6	
SME_8oSdb_F56:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $C, 0, 3, $FF, $E6	
		dc.b $FC, $D, 0, 7, $FF, $EE	
		dc.b $C, 8, 0, $F, $FF, $F6	
SME_8oSdb_F70:	dc.b 0, 2	
		dc.b $EC, $D, 0, 0, $FF, $EA	
		dc.b $FC, $A, 0, 8, $FF, $F2	
SME_8oSdb_F7E:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $EF	
		dc.b $F4, $D, 0, 3, $FF, $EF	
		dc.b 4, 9, 0, $B, $FF, $EF	
SME_8oSdb_F92:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $EF	
		dc.b $F4, $D, 0, 3, $FF, $EF	
		dc.b 4, 9, 0, $B, $FF, $EF	
SME_8oSdb_FA6:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $EF	
		dc.b $F4, $D, 0, 3, $FF, $EF	
		dc.b 4, 9, 0, $B, $FF, $EF	
SME_8oSdb_FBA:	dc.b 0, 3	
		dc.b $E6, $B, 0, 0, $FF, $F5	
		dc.b 6, 8, 0, $C, $FF, $F5	
		dc.b $E, 5, 0, $F, $FF, $F5	
SME_8oSdb_FCE:	dc.b 0, 3	
		dc.b $E6, $B, 0, 0, $FF, $F5	
		dc.b 6, 8, 0, $C, $FF, $F5	
		dc.b $E, 5, 0, $F, $FF, $F5	
SME_8oSdb_FE2:	dc.b 0, 3	
		dc.b $EC, 4, 0, 0, $FF, $FD	
		dc.b $F4, $A, 0, 2, $FF, $F5	
		dc.b $C, $C, 0, $B, $FF, $ED	
SME_8oSdb_FF6:	dc.b 0, 3	
		dc.b $EC, 4, 0, 0, $FF, $FD	
		dc.b $F4, $A, 0, 2, $FF, $F5	
		dc.b $C, 4, 0, $B, $FF, $F5	
SME_8oSdb_100A:	dc.b 0, 3	
		dc.b $EC, 4, 0, 0, $FF, $FD	
		dc.b $F4, 9, 0, 2, $FF, $F5	
		dc.b 4, $D, 0, 8, $FF, $ED	
SME_8oSdb_101E:	dc.b 0, 3	
		dc.b $EC, 4, 0, 0, $FF, $FD	
		dc.b $F4, $A, 0, 2, $FF, $F5	
		dc.b $C, 4, 0, $B, $FF, $F5	
SME_8oSdb_1032:	dc.b 0, 4	
		dc.b $FC, $A, 0, 0, $FF, $F8	
		dc.b $EC, 8, 0, 9, $FF, $F0	
		dc.b $F4, $C, 0, $C, $FF, $F0	
		dc.b $FC, 0, 0, $10, $FF, $F0	
SME_8oSdb_104C:	dc.b 0, 3	
		dc.b $ED, 8, 0, 0, $FF, $EE	
		dc.b $F5, $D, 0, 3, $FF, $EE	
		dc.b 5, 9, 0, $B, $FF, $F6	
SME_8oSdb_1060:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $D, 0, 3, $FF, $EE	
		dc.b 4, 8, 0, $B, $FF, $EE	
		dc.b $C, 8, 0, $E, $FF, $F6	
SME_8oSdb_107A:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $D, 0, 3, $FF, $EE	
		dc.b 4, 8, 0, $B, $FF, $EE	
		dc.b $C, 8, 0, $E, $FF, $F6	
SME_8oSdb_1094:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $D, 0, 3, $FF, $EE	
		dc.b 4, 8, 0, $B, $FF, $EE	
		dc.b $C, 8, 0, $E, $FF, $F6	
SME_8oSdb_10AE:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $D, 0, 3, $FF, $EE	
		dc.b 4, 8, 0, $B, $FF, $EE	
		dc.b $C, 8, 0, $E, $FF, $F6	
SME_8oSdb_10C8:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $D, 0, 3, $FF, $EE	
		dc.b 4, 8, 0, $B, $FF, $EE	
		dc.b $C, 8, 0, $E, $FF, $F6	
SME_8oSdb_10E2:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F3	
		dc.b $C, $C, 0, $C, $FF, $F3	
SME_8oSdb_10F0:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $F3	
		dc.b $F4, $D, 0, 3, $FF, $F3	
		dc.b 4, 8, 0, $B, $FF, $F3	
		dc.b $C, $C, 0, $E, $FF, $F3	
SME_8oSdb_110A:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $ED	
		dc.b $F4, $D, 0, 3, $FF, $ED	
		dc.b 4, 8, 0, $B, $FF, $F5	
		dc.b $C, 8, 0, $E, $FF, $F5	
SME_8oSdb_1124:	dc.b 0, 4	
		dc.b $EE, 8, 0, 0, $FF, $ED	
		dc.b $F6, $C, 0, 3, $FF, $E5	
		dc.b $FE, $C, 0, 7, $FF, $ED	
		dc.b 6, 9, 0, $B, $FF, $F5	
SME_8oSdb_113E:	dc.b 0, 3	
		dc.b $EC, $E, 0, 0, $FF, $ED	
		dc.b 4, 8, 0, $C, $FF, $F5	
		dc.b $C, 8, 0, $F, $FF, $F5	
SME_8oSdb_1152:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $EE	
		dc.b $F4, $D, 0, 3, $FF, $EE	
		dc.b 4, 8, 0, $B, $FF, $F6	
		dc.b $C, 8, 0, $E, $FF, $F6	
SME_8oSdb_116C:	dc.b 0, 4	
		dc.b $EC, 8, 0, 0, $FF, $F5	
		dc.b $F4, $C, 0, 3, $FF, $ED	
		dc.b $FC, 9, 0, 7, $FF, $F5	
		dc.b $C, 8, 0, $D, $FF, $F5	
SME_8oSdb_1186:	dc.b 0, 3	
		dc.b $F4, $A, 0, 0, $FF, $FB	
		dc.b $FC, 4, 0, 9, $FF, $EB	
		dc.b 4, 8, 0, $B, $FF, $E3	
SME_8oSdb_119A:	dc.b 0, 3	
		dc.b $F4, 9, 0, 0, $FF, $F4	
		dc.b 4, $C, 0, 6, $FF, $EC	
		dc.b 4, 0, 0, $A, 0, $C	
SME_8oSdb_11AE:	dc.b 0, 2	
		dc.b $F4, 9, 0, 0, $FF, $F7	
		dc.b 4, $C, 0, 6, $FF, $EF	
SME_8oSdb_11BC:	dc.b 0, 3	
		dc.b $F4, 9, 8, 0, $FF, $F3	
		dc.b 4, $C, 8, 6, $FF, $F3	
		dc.b 4, 0, 8, $A, $FF, $EB	
SME_8oSdb_11D0:	dc.b 0, 3	
		dc.b $F4, $A, 8, 0, $FF, $EE	
		dc.b $FC, 4, 8, 9, 0, 6	
		dc.b 4, 8, 8, $B, 0, 6	
SME_8oSdb_11E4:	dc.b 0, 2	
		dc.b $F4, $E, 0, 0, $FF, $F2	
		dc.b 4, 0, 0, $C, $FF, $EA	
SME_8oSdb_11F2:	dc.b 0, 2	
		dc.b $F4, 9, 0, 0, $FF, $F7	
		dc.b 4, $C, 0, 6, $FF, $EF	
SME_8oSdb_1200:	dc.b 0, 2	
		dc.b $F4, $E, 8, 0, $FF, $EF	
		dc.b 4, 0, 8, $C, 0, $F	
SME_8oSdb_120E:	dc.b 0, 3	
		dc.b $F8, $E, 0, 0, $FF, $EC	
		dc.b $F8, 2, 0, $C, 0, $C	
		dc.b $10, 8, 0, $F, $FF, $F4	
SME_8oSdb_1222:	dc.b 0, 3	
		dc.b $F7, $E, 0, 0, $FF, $EC	
		dc.b $F7, 2, 0, $C, 0, $C	
		dc.b $F, 8, 0, $F, $FF, $F4	
SME_8oSdb_1236:	dc.b 0, 1	
		dc.b $F0, $B, 0, 0, $FF, $F4	
SME_8oSdb_123E:	dc.b 0, 3	
		dc.b $E8, 9, 0, 0, $FF, $F3	
		dc.b $F8, $D, 0, 6, $FF, $F3	
		dc.b 8, 9, 0, $E, $FF, $F3	
SME_8oSdb_1252:	dc.b 0, 2	
		dc.b $E8, $F, 0, 0, $FF, $F0	
		dc.b 8, 9, 0, $10, $FF, $F0	
SME_8oSdb_1260:	dc.b 0, 3	
		dc.b $E8, $A, 0, 0, $FF, $F0	
		dc.b 0, $C, 0, 9, $FF, $F0	
		dc.b 8, 9, 0, $D, $FF, $F0	
SME_8oSdb_1274:	dc.b 0, 3	
		dc.b $E8, $A, 0, 0, $FF, $F0	
		dc.b 0, $C, 0, 9, $FF, $F0	
		dc.b 8, 9, 0, $D, $FF, $F0	
SME_8oSdb_1288:	dc.b 0, 3	
		dc.b $F4, $E, 0, 0, $FF, $E8	
		dc.b $F4, 2, 0, $C, 0, 8	
		dc.b $C, 8, 0, $F, $FF, $F0	
SME_8oSdb_129C:	dc.b 0, 3	
		dc.b $F3, $E, 0, 0, $FF, $E8	
		dc.b $F3, 2, 0, $C, 0, 8	
		dc.b $B, 8, 0, $F, $FF, $F0	
SME_8oSdb_12B0:	dc.b 0, 4	
		dc.b $EC, $E, 0, 0, $FF, $E8	
		dc.b $FC, 0, 0, $C, 0, 8	
		dc.b 4, $C, 0, $D, $FF, $F0	
		dc.b $C, 8, 0, $11, $FF, $F8	
SME_8oSdb_12CA:	dc.b 0, 3	
		dc.b $EC, 9, 0, 0, $FF, $F0	
		dc.b $FC, $D, 0, 6, $FF, $F0	
		dc.b $C, 4, 0, $E, $FF, $F8	
SME_8oSdb_12DE:	dc.b 0, 3	
		dc.b $EC, 9, 0, 0, $FF, $F0	
		dc.b $FC, $D, 0, 6, $FF, $F0	
		dc.b $C, 4, 0, $E, $FF, $F8	
SME_8oSdb_12F2:	dc.b 0, 3	
		dc.b $EC, 9, 0, 0, $FF, $F0	
		dc.b $FC, $D, 0, 6, $FF, $F0	
		dc.b $C, 4, 0, $E, $FF, $F8	
SME_8oSdb_1306:	dc.b 0, 3	
		dc.b $EC, 9, 0, 0, $FF, $F0	
		dc.b $FC, $D, 0, 6, $FF, $F0	
		dc.b $C, 4, 0, $E, $FF, $F8	
SME_8oSdb_131A:	dc.b 0, 3	
		dc.b $EC, 9, 0, 0, $FF, $F0	
		dc.b $FC, $D, 0, 6, $FF, $F0	
		dc.b $C, 4, 0, $E, $FF, $F8	
SME_8oSdb_132E:	dc.b 0, 3	
		dc.b $EC, 9, 0, 0, $FF, $F0	
		dc.b $FC, $D, 0, 6, $FF, $F0	
		dc.b $C, 4, 0, $E, $FF, $F8	
SME_8oSdb_1342:	dc.b 0, 1	
		dc.b $F0, $B, 0, 0, $FF, $F4	
SME_8oSdb_134A:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F4	
		dc.b $C, 8, 0, $C, $FF, $F4	
SME_8oSdb_1358:	dc.b 0, 4	
		dc.b $EC, 9, 0, 0, $FF, $F0	
		dc.b $FC, $C, 0, 6, $FF, $F0	
		dc.b 4, 8, 0, $A, $FF, $F8	
		dc.b $C, 4, 0, $D, 0, 0	
SME_8oSdb_1372:	dc.b 0, 4	
		dc.b $E8, $B, 0, 0, $FF, $EC	
		dc.b $F8, 5, 0, $C, 0, 4	
		dc.b 8, 8, 0, $10, $FF, $EC	
		dc.b $10, 4, 0, $13, $FF, $EC	
SME_8oSdb_138C:	dc.b 0, 4	
		dc.b $E8, 9, 0, 0, $FF, $F0	
		dc.b $F8, $D, 0, 6, $FF, $F0	
		dc.b 8, 4, 0, $E, $FF, $F8	
		dc.b $10, 8, 0, $10, $FF, $F8	
SME_8oSdb_13A6:	dc.b 0, 4	
		dc.b $E8, 9, 0, 0, $FF, $F0	
		dc.b $F8, $C, 0, 6, $FF, $F0	
		dc.b 0, 8, 0, $A, $FF, $F0	
		dc.b 8, 9, 0, $D, $FF, $F8	
SME_8oSdb_13C0:	dc.b 0, 3	
		dc.b $E8, $B, 0, 0, $FF, $F0	
		dc.b 8, 4, 0, $C, $FF, $F8	
		dc.b $10, 8, 0, $E, $FF, $F8	
SME_8oSdb_13D4:	dc.b 0, 2	
		dc.b $E8, $B, 0, 0, $FF, $F4	
		dc.b 8, 9, 0, $C, $FF, $F4	
SME_8oSdb_13E2:	dc.b 0, 4	
		dc.b $E8, 9, 0, 0, $FF, $F8	
		dc.b $F8, $C, 0, 6, $FF, $F0	
		dc.b 0, 8, 0, $A, $FF, $F8	
		dc.b 8, 9, 0, $D, $FF, $F0	
SME_8oSdb_13FC:	dc.b 0, 3	
		dc.b $E8, $C, 0, 0, $FF, $F0	
		dc.b $F0, $A, 0, 4, $FF, $F8	
		dc.b 8, 9, 0, $D, $FF, $F0	
SME_8oSdb_1410:	dc.b 0, 2	
		dc.b $E8, $B, 0, 0, $FF, $F4	
		dc.b 8, 9, 0, $C, $FF, $F4	
SME_8oSdb_141E:	dc.b 0, 4	
		dc.b $E8, 9, 0, 0, $FF, $EC	
		dc.b $F8, $C, 0, 6, $FF, $EC	
		dc.b 0, 8, 0, $A, $FF, $EC	
		dc.b 8, $D, 0, $D, $FF, $F4	
SME_8oSdb_1438:	dc.b 0, 3	
		dc.b $EC, 4, 0, 0, 0, 0	
		dc.b $F4, $F, 0, 2, $FF, $F8	
		dc.b $FC, 6, 0, $12, $FF, $E8	
SME_8oSdb_144C:	dc.b 0, 3	
		dc.b $E8, 9, 0, 0, $FF, $F0	
		dc.b $F8, $D, 0, 6, $FF, $F0	
		dc.b 8, 9, 0, $E, $FF, $F8	
SME_8oSdb_1460:	dc.b 0, 4	
		dc.b $EC, 4, 0, 0, 0, 0	
		dc.b $F4, $D, 0, 2, $FF, $E8	
		dc.b $F4, 5, 0, $A, 0, 8	
		dc.b 4, 5, 0, $E, $FF, $F0	
SME_8oSdb_147A:	dc.b 0, 2	
		dc.b $EC, $F, 0, 0, $FF, $F0	
		dc.b $C, 8, 0, $10, $FF, $F0	
SME_8oSdb_1488:	dc.b 0, 3	
		dc.b $EC, $E, 0, 0, $FF, $F0	
		dc.b 4, 8, 0, $C, $FF, $F0	
		dc.b $C, 4, 0, $F, $FF, $F8	
SME_8oSdb_149C:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F4	
		dc.b $C, 8, 0, $C, $FF, $F4	
SME_8oSdb_14AA:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F4	
		dc.b $C, 0, 0, $C, $FF, $FC	
SME_8oSdb_14B8:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F4	
		dc.b $C, 8, 0, $C, $FF, $F4	
SME_8oSdb_14C6:	dc.b 0, 4	
		dc.b $EC, $D, 0, 0, $FF, $F0	
		dc.b $FC, 8, 0, 8, $FF, $F0	
		dc.b 4, $C, 0, $B, $FF, $F0	
		dc.b $C, 8, 0, $F, $FF, $F8	
SME_8oSdb_14E0:	dc.b 0, 3	
		dc.b $EC, 8, 0, 0, $FF, $F0	
		dc.b $F4, $E, 0, 3, $FF, $F0	
		dc.b $C, 8, 0, $F, $FF, $F0	
SME_8oSdb_14F4:	dc.b 0, 2	
		dc.b $EC, $F, 0, 0, $FF, $F0	
		dc.b $C, 8, 0, $10, $FF, $F8	
SME_8oSdb_1502:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F4	
		dc.b $C, 4, 0, $C, $FF, $F4	
SME_8oSdb_1510:	dc.b 0, 3	
		dc.b $EC, $A, 0, 0, $FF, $F4	
		dc.b 4, 4, 0, 9, $FF, $FC	
		dc.b $C, 8, 0, $B, $FF, $F4	
SME_8oSdb_1524:	dc.b 0, 2	
		dc.b $EC, $B, 0, 0, $FF, $F4	
		dc.b $C, 4, 0, $C, $FF, $FC	
SME_8oSdb_1532:	dc.b 0, 2	
		dc.b $EC, $E, 0, 0, $FF, $F0	
		dc.b 4, 9, 0, $C, $FF, $F0	
SME_8oSdb_1540:	dc.b 0, 7	
		dc.b $C, $C, 0, 0, $FF, $F0	
		dc.b $C, 0, 0, 4, 0, $10	
		dc.b 4, $C, 0, 5, $FF, $F0	
		dc.b 4, 0, 0, 9, 0, $10	
		dc.b $FC, $C, 0, $A, $FF, $F0	
		dc.b $FC, 0, 0, $E, 0, $10	
		dc.b $F4, $C, 0, $F, $FF, $F8	
SME_8oSdb_156C:	dc.b 0, 7	
		dc.b $C, $C, 0, 0, $FF, $F0	
		dc.b $C, 0, 0, 4, 0, $10	
		dc.b 4, $C, 0, 5, $FF, $F0	
		dc.b 4, 0, 0, 9, 0, $10	
		dc.b $FC, $C, 0, $A, $FF, $F0	
		dc.b $FC, 0, 0, $E, 0, $10	
		dc.b $F4, $C, 0, $F, $FF, $F8	
SME_8oSdb_1598:	dc.b 0, 7	
		dc.b $C, $C, 0, 0, $FF, $F0	
		dc.b $C, 0, 0, 4, 0, $10	
		dc.b 4, $C, 0, 5, $FF, $F0	
		dc.b 4, 0, 0, 9, 0, $10	
		dc.b $FC, $C, 0, $A, $FF, $F0	
		dc.b $FC, 0, 0, $E, 0, $10	
		dc.b $F4, $C, 0, $F, $FF, $F8	
SME_8oSdb_15C4:	dc.b 0, 7	
		dc.b $C, $C, 0, 0, $FF, $F0	
		dc.b $C, 0, 0, 4, 0, $10	
		dc.b 4, $C, 0, 5, $FF, $F0	
		dc.b 4, 0, 0, 9, 0, $10	
		dc.b $FC, $C, 0, $A, $FF, $F0	
		dc.b $FC, 0, 0, $E, 0, $10	
		dc.b $F4, $C, 0, $F, $FF, $F8	
SME_8oSdb_15F0:	dc.b 0, 6	
		dc.b $C, 8, 0, 0, $FF, $F8	
		dc.b $C, 0, 0, 3, 0, $10	
		dc.b 4, $C, 0, 4, $FF, $F8	
		dc.b $FC, $C, 0, 8, $FF, $F8	
		dc.b $F4, $C, 0, $C, $FF, $F8	
		dc.b $EC, $C, 0, $10, $FF, $F8	
SME_8oSdb_1616:	dc.b 0, 5	
		dc.b $D, 8, 0, 0, $FF, $F8	
		dc.b 5, $C, 0, 3, $FF, $F8	
		dc.b $FD, $C, 0, 7, $FF, $F8	
		dc.b $F5, $C, 0, $B, $FF, $F8	
		dc.b $ED, $C, 0, $F, $FF, $F8	
SME_8oSdb_1636:	dc.b 0, 6	
		dc.b $C, 8, 0, 0, $FF, $F8	
		dc.b $C, 0, 0, 3, 0, $10	
		dc.b 4, $C, 0, 4, $FF, $F8	
		dc.b $FC, $C, 0, 8, $FF, $F8	
		dc.b $F4, $C, 0, $C, $FF, $F8	
		dc.b $EC, $C, 0, $10, $FF, $F8	
SME_8oSdb_165C:	dc.b 0, 5	
		dc.b $D, 8, 0, 0, $FF, $F8	
		dc.b 5, $C, 0, 3, $FF, $F8	
		dc.b $FD, $C, 0, 7, $FF, $F8	
		dc.b $F5, $C, 0, $B, $FF, $F8	
		dc.b $ED, $C, 0, $F, $FF, $F8	
SME_8oSdb_167C:	dc.b 0, 5	
		dc.b $D, 8, 0, 0, 0, 0	
		dc.b 5, $C, 0, 3, $FF, $F8	
		dc.b $FD, $C, 0, 7, $FF, $F8	
		dc.b $F5, $C, 0, $B, $FF, $F8	
		dc.b $ED, $C, 0, $F, $FF, $F8	
SME_8oSdb_169C:	dc.b 0, 5	
		dc.b $D, 8, 0, 0, 0, 0	
		dc.b 5, $C, 0, 3, $FF, $F8	
		dc.b $FD, $C, 0, 7, $FF, $F8	
		dc.b $F5, $C, 0, $B, $FF, $F8	
		dc.b $ED, $C, 0, $F, $FF, $F8	
SME_8oSdb_16BC:	dc.b 0, 5	
		dc.b $D, 8, 0, 0, 0, 0	
		dc.b 5, $C, 0, 3, $FF, $F8	
		dc.b $FD, $C, 0, 7, $FF, $F8	
		dc.b $F5, $C, 0, $B, $FF, $F8	
		dc.b $ED, $C, 0, $F, $FF, $F8	
SME_8oSdb_16DC:	dc.b 0, 5	
		dc.b $D, 8, 0, 0, 0, 0	
		dc.b 5, $C, 0, 3, $FF, $F8	
		dc.b $FD, $C, 0, 7, $FF, $F8	
		dc.b $F5, $C, 0, $B, $FF, $F8	
		dc.b $ED, $C, 0, $F, $FF, $F8	
SME_8oSdb_16FC:	dc.b 0, 9	
		dc.b $D, $C, 0, 0, $FF, $F8	
		dc.b $D, 0, 0, 4, 0, $18	
		dc.b 5, $C, 0, 5, $FF, $F8	
		dc.b 5, 0, 0, 9, 0, $18	
		dc.b $FD, $C, 0, $A, $FF, $F8	
		dc.b $FD, 0, 0, $E, 0, $18	
		dc.b $F5, $C, 0, $F, $FF, $F8	
		dc.b $F5, 0, 0, $13, 0, $18	
		dc.b $ED, 4, 0, $14, 0, 8	
SME_8oSdb_1734:	dc.b 0, 7	
		dc.b $D, $C, 0, 0, 0, 1	
		dc.b 5, $C, 0, 4, $FF, $F9	
		dc.b 5, 0, 0, 8, 0, $19	
		dc.b $FD, $C, 0, 9, $FF, $F9	
		dc.b $FD, 0, 0, $D, 0, $19	
		dc.b $F5, $C, 0, $E, $FF, $F9	
		dc.b $ED, 8, 0, $12, 0, 1	
SME_8oSdb_1760:	dc.b 0, 9	
		dc.b $D, $C, 0, 0, $FF, $F8	
		dc.b $D, 0, 0, 4, 0, $18	
		dc.b 5, $C, 0, 5, $FF, $F8	
		dc.b 5, 0, 0, 9, 0, $18	
		dc.b $FD, $C, 0, $A, $FF, $F8	
		dc.b $FD, 0, 0, $E, 0, $18	
		dc.b $F5, $C, 0, $F, $FF, $F8	
		dc.b $F5, 0, 0, $13, 0, $18	
		dc.b $ED, 4, 0, $14, 0, 8	
SME_8oSdb_1798:	dc.b 0, 7	
		dc.b $D, $C, 0, 0, 0, 1	
		dc.b 5, $C, 0, 4, $FF, $F9	
		dc.b 5, 0, 0, 8, 0, $19	
		dc.b $FD, $C, 0, 9, $FF, $F9	
		dc.b $FD, 0, 0, $D, 0, $19	
		dc.b $F5, $C, 0, $E, $FF, $F9	
		dc.b $ED, 8, 0, $12, 0, 1	
		even