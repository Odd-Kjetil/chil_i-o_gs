; --------------------------------------------------------------------------------
; Dynamic Pattern Loading Cues - output from SonMapEd - Sonic 3 & Knuckles format; --------------------------------------------------------------------------------

SME_JtC8o:	
		dc.w SME_JtC8o_3CC-SME_JtC8o, SME_JtC8o_3CE-SME_JtC8o	
		dc.w SME_JtC8o_3D6-SME_JtC8o, SME_JtC8o_3DE-SME_JtC8o	
		dc.w SME_JtC8o_3E6-SME_JtC8o, SME_JtC8o_3EC-SME_JtC8o	
		dc.w SME_JtC8o_3F4-SME_JtC8o, SME_JtC8o_3FC-SME_JtC8o	
		dc.w SME_JtC8o_404-SME_JtC8o, SME_JtC8o_40A-SME_JtC8o	
		dc.w SME_JtC8o_414-SME_JtC8o, SME_JtC8o_41E-SME_JtC8o	
		dc.w SME_JtC8o_428-SME_JtC8o, SME_JtC8o_432-SME_JtC8o	
		dc.w SME_JtC8o_43C-SME_JtC8o, SME_JtC8o_446-SME_JtC8o	
		dc.w SME_JtC8o_450-SME_JtC8o, SME_JtC8o_45A-SME_JtC8o	
		dc.w SME_JtC8o_462-SME_JtC8o, SME_JtC8o_46A-SME_JtC8o	
		dc.w SME_JtC8o_472-SME_JtC8o, SME_JtC8o_478-SME_JtC8o	
		dc.w SME_JtC8o_480-SME_JtC8o, SME_JtC8o_48A-SME_JtC8o	
		dc.w SME_JtC8o_492-SME_JtC8o, SME_JtC8o_498-SME_JtC8o	
		dc.w SME_JtC8o_4A2-SME_JtC8o, SME_JtC8o_4AC-SME_JtC8o	
		dc.w SME_JtC8o_4B6-SME_JtC8o, SME_JtC8o_4C0-SME_JtC8o	
		dc.w SME_JtC8o_4CA-SME_JtC8o, SME_JtC8o_4D4-SME_JtC8o	
		dc.w SME_JtC8o_4DE-SME_JtC8o, SME_JtC8o_4E8-SME_JtC8o	
		dc.w SME_JtC8o_4EE-SME_JtC8o, SME_JtC8o_4F4-SME_JtC8o	
		dc.w SME_JtC8o_4FA-SME_JtC8o, SME_JtC8o_500-SME_JtC8o	
		dc.w SME_JtC8o_50A-SME_JtC8o, SME_JtC8o_510-SME_JtC8o	
		dc.w SME_JtC8o_51A-SME_JtC8o, SME_JtC8o_520-SME_JtC8o	
		dc.w SME_JtC8o_526-SME_JtC8o, SME_JtC8o_52C-SME_JtC8o	
		dc.w SME_JtC8o_532-SME_JtC8o, SME_JtC8o_538-SME_JtC8o	
		dc.w SME_JtC8o_540-SME_JtC8o, SME_JtC8o_546-SME_JtC8o	
		dc.w SME_JtC8o_54E-SME_JtC8o, SME_JtC8o_554-SME_JtC8o	
		dc.w SME_JtC8o_55C-SME_JtC8o, SME_JtC8o_566-SME_JtC8o	
		dc.w SME_JtC8o_570-SME_JtC8o, SME_JtC8o_57A-SME_JtC8o	
		dc.w SME_JtC8o_584-SME_JtC8o, SME_JtC8o_58C-SME_JtC8o	
		dc.w SME_JtC8o_592-SME_JtC8o, SME_JtC8o_598-SME_JtC8o	
		dc.w SME_JtC8o_5A0-SME_JtC8o, SME_JtC8o_5AA-SME_JtC8o	
		dc.w SME_JtC8o_5B0-SME_JtC8o, SME_JtC8o_5B6-SME_JtC8o	
		dc.w SME_JtC8o_5BE-SME_JtC8o, SME_JtC8o_5C2-SME_JtC8o	
		dc.w SME_JtC8o_5CA-SME_JtC8o, SME_JtC8o_5D4-SME_JtC8o	
		dc.w SME_JtC8o_5DE-SME_JtC8o, SME_JtC8o_5E8-SME_JtC8o	
		dc.w SME_JtC8o_5EE-SME_JtC8o, SME_JtC8o_5F2-SME_JtC8o	
		dc.w SME_JtC8o_5F8-SME_JtC8o, SME_JtC8o_5FC-SME_JtC8o	
		dc.w SME_JtC8o_602-SME_JtC8o, SME_JtC8o_60A-SME_JtC8o	
		dc.w SME_JtC8o_614-SME_JtC8o, SME_JtC8o_61E-SME_JtC8o	
		dc.w SME_JtC8o_626-SME_JtC8o, SME_JtC8o_62E-SME_JtC8o	
		dc.w SME_JtC8o_636-SME_JtC8o, SME_JtC8o_63A-SME_JtC8o	
		dc.w SME_JtC8o_644-SME_JtC8o, SME_JtC8o_64E-SME_JtC8o	
		dc.w SME_JtC8o_654-SME_JtC8o, SME_JtC8o_65C-SME_JtC8o	
		dc.w SME_JtC8o_664-SME_JtC8o, SME_JtC8o_66A-SME_JtC8o	
		dc.w SME_JtC8o_674-SME_JtC8o, SME_JtC8o_67C-SME_JtC8o	
		dc.w SME_JtC8o_682-SME_JtC8o, SME_JtC8o_688-SME_JtC8o	
		dc.w SME_JtC8o_692-SME_JtC8o, SME_JtC8o_69A-SME_JtC8o	
		dc.w SME_JtC8o_6A0-SME_JtC8o, SME_JtC8o_6A8-SME_JtC8o	
		dc.w SME_JtC8o_6AC-SME_JtC8o, SME_JtC8o_6B2-SME_JtC8o	
		dc.w SME_JtC8o_6BA-SME_JtC8o, SME_JtC8o_6BE-SME_JtC8o	
		dc.w SME_JtC8o_6C4-SME_JtC8o, SME_JtC8o_6CA-SME_JtC8o	
		dc.w SME_JtC8o_6D2-SME_JtC8o, SME_JtC8o_6D6-SME_JtC8o	
		dc.w SME_JtC8o_6DC-SME_JtC8o, SME_JtC8o_6E4-SME_JtC8o	
		dc.w SME_JtC8o_6E8-SME_JtC8o, SME_JtC8o_6EE-SME_JtC8o	
		dc.w SME_JtC8o_6F4-SME_JtC8o, SME_JtC8o_6FE-SME_JtC8o	
		dc.w SME_JtC8o_706-SME_JtC8o, SME_JtC8o_70E-SME_JtC8o	
		dc.w SME_JtC8o_714-SME_JtC8o, SME_JtC8o_71C-SME_JtC8o	
		dc.w SME_JtC8o_724-SME_JtC8o, SME_JtC8o_72C-SME_JtC8o	
		dc.w SME_JtC8o_732-SME_JtC8o, SME_JtC8o_73C-SME_JtC8o	
		dc.w SME_JtC8o_744-SME_JtC8o, SME_JtC8o_74C-SME_JtC8o	
		dc.w SME_JtC8o_752-SME_JtC8o, SME_JtC8o_75A-SME_JtC8o	
		dc.w SME_JtC8o_75E-SME_JtC8o, SME_JtC8o_768-SME_JtC8o	
		dc.w SME_JtC8o_772-SME_JtC8o, SME_JtC8o_77A-SME_JtC8o	
		dc.w SME_JtC8o_784-SME_JtC8o, SME_JtC8o_78E-SME_JtC8o	
		dc.w SME_JtC8o_798-SME_JtC8o, SME_JtC8o_7A0-SME_JtC8o	
		dc.w SME_JtC8o_7AA-SME_JtC8o, SME_JtC8o_7B4-SME_JtC8o	
		dc.w SME_JtC8o_7BE-SME_JtC8o, SME_JtC8o_7C6-SME_JtC8o	
		dc.w SME_JtC8o_7D0-SME_JtC8o, SME_JtC8o_7D8-SME_JtC8o	
		dc.w SME_JtC8o_7DE-SME_JtC8o, SME_JtC8o_7E6-SME_JtC8o	
		dc.w SME_JtC8o_7EC-SME_JtC8o, SME_JtC8o_7F2-SME_JtC8o	
		dc.w SME_JtC8o_7F8-SME_JtC8o, SME_JtC8o_7FE-SME_JtC8o	
		dc.w SME_JtC8o_804-SME_JtC8o, SME_JtC8o_80C-SME_JtC8o	
		dc.w SME_JtC8o_814-SME_JtC8o, SME_JtC8o_81A-SME_JtC8o	
		dc.w SME_JtC8o_822-SME_JtC8o, SME_JtC8o_82C-SME_JtC8o	
		dc.w SME_JtC8o_836-SME_JtC8o, SME_JtC8o_840-SME_JtC8o	
		dc.w SME_JtC8o_846-SME_JtC8o, SME_JtC8o_852-SME_JtC8o	
		dc.w SME_JtC8o_85E-SME_JtC8o, SME_JtC8o_862-SME_JtC8o	
		dc.w SME_JtC8o_866-SME_JtC8o, SME_JtC8o_86A-SME_JtC8o	
		dc.w SME_JtC8o_86E-SME_JtC8o, SME_JtC8o_872-SME_JtC8o	
		dc.w SME_JtC8o_878-SME_JtC8o, SME_JtC8o_87E-SME_JtC8o	
		dc.w SME_JtC8o_888-SME_JtC8o, SME_JtC8o_892-SME_JtC8o	
		dc.w SME_JtC8o_89C-SME_JtC8o, SME_JtC8o_8A4-SME_JtC8o	
		dc.w SME_JtC8o_8AC-SME_JtC8o, SME_JtC8o_8B6-SME_JtC8o	
		dc.w SME_JtC8o_8BE-SME_JtC8o, SME_JtC8o_8C8-SME_JtC8o	
		dc.w SME_JtC8o_8D0-SME_JtC8o, SME_JtC8o_8DA-SME_JtC8o	
		dc.w SME_JtC8o_8E2-SME_JtC8o, SME_JtC8o_8EA-SME_JtC8o	
		dc.w SME_JtC8o_8F2-SME_JtC8o, SME_JtC8o_8FA-SME_JtC8o	
		dc.w SME_JtC8o_902-SME_JtC8o, SME_JtC8o_90C-SME_JtC8o	
		dc.w SME_JtC8o_916-SME_JtC8o, SME_JtC8o_920-SME_JtC8o	
		dc.w SME_JtC8o_92A-SME_JtC8o, SME_JtC8o_930-SME_JtC8o	
		dc.w SME_JtC8o_938-SME_JtC8o, SME_JtC8o_940-SME_JtC8o	
		dc.w SME_JtC8o_948-SME_JtC8o, SME_JtC8o_950-SME_JtC8o	
		dc.w SME_JtC8o_958-SME_JtC8o, SME_JtC8o_960-SME_JtC8o	
		dc.w SME_JtC8o_968-SME_JtC8o, SME_JtC8o_970-SME_JtC8o	
		dc.w SME_JtC8o_978-SME_JtC8o, SME_JtC8o_982-SME_JtC8o	
		dc.w SME_JtC8o_98A-SME_JtC8o, SME_JtC8o_994-SME_JtC8o	
		dc.w SME_JtC8o_99E-SME_JtC8o, SME_JtC8o_9A8-SME_JtC8o	
		dc.w SME_JtC8o_9B2-SME_JtC8o, SME_JtC8o_9BC-SME_JtC8o	
		dc.w SME_JtC8o_9C2-SME_JtC8o, SME_JtC8o_9CC-SME_JtC8o	
		dc.w SME_JtC8o_9D6-SME_JtC8o, SME_JtC8o_9E0-SME_JtC8o	
		dc.w SME_JtC8o_9E8-SME_JtC8o, SME_JtC8o_9F2-SME_JtC8o	
		dc.w SME_JtC8o_9FC-SME_JtC8o, SME_JtC8o_A04-SME_JtC8o	
		dc.w SME_JtC8o_A0C-SME_JtC8o, SME_JtC8o_A12-SME_JtC8o	
		dc.w SME_JtC8o_A1A-SME_JtC8o, SME_JtC8o_A22-SME_JtC8o	
		dc.w SME_JtC8o_A28-SME_JtC8o, SME_JtC8o_A2E-SME_JtC8o	
		dc.w SME_JtC8o_A34-SME_JtC8o, SME_JtC8o_A3C-SME_JtC8o	
		dc.w SME_JtC8o_A44-SME_JtC8o, SME_JtC8o_A48-SME_JtC8o	
		dc.w SME_JtC8o_A50-SME_JtC8o, SME_JtC8o_A56-SME_JtC8o	
		dc.w SME_JtC8o_A5E-SME_JtC8o, SME_JtC8o_A66-SME_JtC8o	
		dc.w SME_JtC8o_A6E-SME_JtC8o, SME_JtC8o_A76-SME_JtC8o	
		dc.w SME_JtC8o_A80-SME_JtC8o, SME_JtC8o_A88-SME_JtC8o	
		dc.w SME_JtC8o_A90-SME_JtC8o, SME_JtC8o_A98-SME_JtC8o	
		dc.w SME_JtC8o_AA0-SME_JtC8o, SME_JtC8o_AA8-SME_JtC8o	
		dc.w SME_JtC8o_AB0-SME_JtC8o, SME_JtC8o_AB4-SME_JtC8o	
		dc.w SME_JtC8o_ABA-SME_JtC8o, SME_JtC8o_AC4-SME_JtC8o	
		dc.w SME_JtC8o_ACE-SME_JtC8o, SME_JtC8o_AD8-SME_JtC8o	
		dc.w SME_JtC8o_AE2-SME_JtC8o, SME_JtC8o_AEA-SME_JtC8o	
		dc.w SME_JtC8o_AF0-SME_JtC8o, SME_JtC8o_AFA-SME_JtC8o	
		dc.w SME_JtC8o_B02-SME_JtC8o, SME_JtC8o_B08-SME_JtC8o	
		dc.w SME_JtC8o_B12-SME_JtC8o, SME_JtC8o_B1A-SME_JtC8o	
		dc.w SME_JtC8o_B22-SME_JtC8o, SME_JtC8o_B2C-SME_JtC8o	
		dc.w SME_JtC8o_B32-SME_JtC8o, SME_JtC8o_B3A-SME_JtC8o	
		dc.w SME_JtC8o_B40-SME_JtC8o, SME_JtC8o_B46-SME_JtC8o	
		dc.w SME_JtC8o_B4C-SME_JtC8o, SME_JtC8o_B56-SME_JtC8o	
		dc.w SME_JtC8o_B5E-SME_JtC8o, SME_JtC8o_B64-SME_JtC8o	
		dc.w SME_JtC8o_B6A-SME_JtC8o, SME_JtC8o_B72-SME_JtC8o	
		dc.w SME_JtC8o_B78-SME_JtC8o, SME_JtC8o_B7E-SME_JtC8o	
		dc.w SME_JtC8o_B8E-SME_JtC8o, SME_JtC8o_B9E-SME_JtC8o	
		dc.w SME_JtC8o_BAE-SME_JtC8o, SME_JtC8o_BBE-SME_JtC8o	
		dc.w SME_JtC8o_BCC-SME_JtC8o, SME_JtC8o_BD8-SME_JtC8o	
		dc.w SME_JtC8o_BE6-SME_JtC8o, SME_JtC8o_BF2-SME_JtC8o	
		dc.w SME_JtC8o_BFE-SME_JtC8o, SME_JtC8o_C0A-SME_JtC8o	
		dc.w SME_JtC8o_C16-SME_JtC8o, SME_JtC8o_C22-SME_JtC8o	
		dc.w SME_JtC8o_C36-SME_JtC8o, SME_JtC8o_C46-SME_JtC8o	
		dc.w SME_JtC8o_C5A-SME_JtC8o, SME_JtC8o_C6A-SME_JtC8o	
		dc.w SME_JtC8o_C6C-SME_JtC8o, SME_JtC8o_C74-SME_JtC8o	
		dc.w SME_JtC8o_C7C-SME_JtC8o, SME_JtC8o_C84-SME_JtC8o	
		dc.w SME_JtC8o_C8C-SME_JtC8o, SME_JtC8o_C96-SME_JtC8o	
		dc.w SME_JtC8o_CA0-SME_JtC8o, SME_JtC8o_CA8-SME_JtC8o	
		dc.w SME_JtC8o_CAE-SME_JtC8o, SME_JtC8o_CB8-SME_JtC8o	
		dc.w SME_JtC8o_CC2-SME_JtC8o, SME_JtC8o_CCC-SME_JtC8o	
		dc.w SME_JtC8o_CD6-SME_JtC8o, SME_JtC8o_CE0-SME_JtC8o	
		dc.w SME_JtC8o_CEA-SME_JtC8o, SME_JtC8o_CF4-SME_JtC8o	
		dc.w SME_JtC8o_CFE-SME_JtC8o, SME_JtC8o_D06-SME_JtC8o	
		dc.w SME_JtC8o_D0E-SME_JtC8o, SME_JtC8o_D16-SME_JtC8o	
		dc.w SME_JtC8o_D1E-SME_JtC8o, SME_JtC8o_D28-SME_JtC8o	
		dc.w SME_JtC8o_D32-SME_JtC8o, SME_JtC8o_D3A-SME_JtC8o	
		dc.w SME_JtC8o_D40-SME_JtC8o, SME_JtC8o_D4A-SME_JtC8o	
		dc.w SME_JtC8o_D54-SME_JtC8o, SME_JtC8o_D5E-SME_JtC8o	
		dc.w SME_JtC8o_D68-SME_JtC8o, SME_JtC8o_D72-SME_JtC8o	
		dc.w SME_JtC8o_D7C-SME_JtC8o, SME_JtC8o_D86-SME_JtC8o	
		dc.w SME_JtC8o_D90-SME_JtC8o, SME_JtC8o_D9A-SME_JtC8o	
		dc.w SME_JtC8o_DA4-SME_JtC8o, SME_JtC8o_DAE-SME_JtC8o	
		dc.w SME_JtC8o_DB8-SME_JtC8o, SME_JtC8o_DC2-SME_JtC8o	
		dc.w SME_JtC8o_DCC-SME_JtC8o, SME_JtC8o_DD6-SME_JtC8o	
		dc.w SME_JtC8o_DE0-SME_JtC8o, SME_JtC8o_DEA-SME_JtC8o	
		dc.w SME_JtC8o_DF4-SME_JtC8o, SME_JtC8o_DFE-SME_JtC8o	
		dc.w SME_JtC8o_E08-SME_JtC8o, SME_JtC8o_E12-SME_JtC8o	
		dc.w SME_JtC8o_E1C-SME_JtC8o, SME_JtC8o_E26-SME_JtC8o	
		dc.w SME_JtC8o_E30-SME_JtC8o, SME_JtC8o_E38-SME_JtC8o	
		dc.w SME_JtC8o_E42-SME_JtC8o, SME_JtC8o_E4C-SME_JtC8o	
		dc.w SME_JtC8o_E56-SME_JtC8o, SME_JtC8o_E60-SME_JtC8o	
		dc.w SME_JtC8o_E68-SME_JtC8o, SME_JtC8o_E6E-SME_JtC8o	
		dc.w SME_JtC8o_E74-SME_JtC8o, SME_JtC8o_E7C-SME_JtC8o	
		dc.w SME_JtC8o_E86-SME_JtC8o, SME_JtC8o_E8C-SME_JtC8o	
		dc.w SME_JtC8o_E92-SME_JtC8o, SME_JtC8o_E9A-SME_JtC8o	
		dc.w SME_JtC8o_E9E-SME_JtC8o, SME_JtC8o_EA6-SME_JtC8o	
		dc.w SME_JtC8o_EB0-SME_JtC8o, SME_JtC8o_EBA-SME_JtC8o	
		dc.w SME_JtC8o_EC4-SME_JtC8o, SME_JtC8o_ECA-SME_JtC8o	
		dc.w SME_JtC8o_ECE-SME_JtC8o, SME_JtC8o_ED4-SME_JtC8o	
		dc.w SME_JtC8o_ED8-SME_JtC8o, SME_JtC8o_EDE-SME_JtC8o	
		dc.w SME_JtC8o_EE6-SME_JtC8o, SME_JtC8o_EF0-SME_JtC8o	
		dc.w SME_JtC8o_EFA-SME_JtC8o, SME_JtC8o_F02-SME_JtC8o	
		dc.w SME_JtC8o_F0A-SME_JtC8o, SME_JtC8o_F12-SME_JtC8o	
		dc.w SME_JtC8o_F16-SME_JtC8o, SME_JtC8o_F20-SME_JtC8o	
		dc.w SME_JtC8o_F2A-SME_JtC8o, SME_JtC8o_F30-SME_JtC8o	
		dc.w SME_JtC8o_F38-SME_JtC8o, SME_JtC8o_F40-SME_JtC8o	
		dc.w SME_JtC8o_F46-SME_JtC8o, SME_JtC8o_F50-SME_JtC8o	
		dc.w SME_JtC8o_F58-SME_JtC8o, SME_JtC8o_F5E-SME_JtC8o	
		dc.w SME_JtC8o_F64-SME_JtC8o, SME_JtC8o_F6E-SME_JtC8o	
		dc.w SME_JtC8o_F76-SME_JtC8o, SME_JtC8o_F7C-SME_JtC8o	
		dc.w SME_JtC8o_F84-SME_JtC8o, SME_JtC8o_F88-SME_JtC8o	
		dc.w SME_JtC8o_F8E-SME_JtC8o, SME_JtC8o_F96-SME_JtC8o	
		dc.w SME_JtC8o_F9A-SME_JtC8o, SME_JtC8o_FA0-SME_JtC8o	
		dc.w SME_JtC8o_FA6-SME_JtC8o, SME_JtC8o_FAE-SME_JtC8o	
		dc.w SME_JtC8o_FB2-SME_JtC8o, SME_JtC8o_FB8-SME_JtC8o	
		dc.w SME_JtC8o_FC0-SME_JtC8o, SME_JtC8o_FC4-SME_JtC8o	
		dc.w SME_JtC8o_FCA-SME_JtC8o, SME_JtC8o_FD0-SME_JtC8o	
		dc.w SME_JtC8o_FDA-SME_JtC8o, SME_JtC8o_FE2-SME_JtC8o	
		dc.w SME_JtC8o_FEA-SME_JtC8o, SME_JtC8o_FF0-SME_JtC8o	
		dc.w SME_JtC8o_FF8-SME_JtC8o, SME_JtC8o_1000-SME_JtC8o	
		dc.w SME_JtC8o_1008-SME_JtC8o, SME_JtC8o_100E-SME_JtC8o	
		dc.w SME_JtC8o_1018-SME_JtC8o, SME_JtC8o_1020-SME_JtC8o	
		dc.w SME_JtC8o_1028-SME_JtC8o, SME_JtC8o_102E-SME_JtC8o	
		dc.w SME_JtC8o_1036-SME_JtC8o, SME_JtC8o_103A-SME_JtC8o	
		dc.w SME_JtC8o_1044-SME_JtC8o, SME_JtC8o_104E-SME_JtC8o	
		dc.w SME_JtC8o_1056-SME_JtC8o, SME_JtC8o_1060-SME_JtC8o	
		dc.w SME_JtC8o_106A-SME_JtC8o, SME_JtC8o_1074-SME_JtC8o	
		dc.w SME_JtC8o_107C-SME_JtC8o, SME_JtC8o_1086-SME_JtC8o	
		dc.w SME_JtC8o_1090-SME_JtC8o, SME_JtC8o_109A-SME_JtC8o	
		dc.w SME_JtC8o_10A2-SME_JtC8o, SME_JtC8o_10AC-SME_JtC8o	
		dc.w SME_JtC8o_10B4-SME_JtC8o, SME_JtC8o_10BA-SME_JtC8o	
		dc.w SME_JtC8o_10C2-SME_JtC8o, SME_JtC8o_10C8-SME_JtC8o	
		dc.w SME_JtC8o_10CE-SME_JtC8o, SME_JtC8o_10D4-SME_JtC8o	
		dc.w SME_JtC8o_10DA-SME_JtC8o, SME_JtC8o_10E0-SME_JtC8o	
		dc.w SME_JtC8o_10E8-SME_JtC8o, SME_JtC8o_10F0-SME_JtC8o	
		dc.w SME_JtC8o_10F6-SME_JtC8o, SME_JtC8o_10FE-SME_JtC8o	
		dc.w SME_JtC8o_1108-SME_JtC8o, SME_JtC8o_1112-SME_JtC8o	
		dc.w SME_JtC8o_111C-SME_JtC8o, SME_JtC8o_1122-SME_JtC8o	
		dc.w SME_JtC8o_112E-SME_JtC8o, SME_JtC8o_113A-SME_JtC8o	
		dc.w SME_JtC8o_113E-SME_JtC8o, SME_JtC8o_1142-SME_JtC8o	
		dc.w SME_JtC8o_1146-SME_JtC8o, SME_JtC8o_114A-SME_JtC8o	
		dc.w SME_JtC8o_114E-SME_JtC8o, SME_JtC8o_1154-SME_JtC8o	
		dc.w SME_JtC8o_115A-SME_JtC8o, SME_JtC8o_1164-SME_JtC8o	
		dc.w SME_JtC8o_116E-SME_JtC8o, SME_JtC8o_1178-SME_JtC8o	
		dc.w SME_JtC8o_1180-SME_JtC8o, SME_JtC8o_118A-SME_JtC8o	
		dc.w SME_JtC8o_1194-SME_JtC8o, SME_JtC8o_119E-SME_JtC8o	
		dc.w SME_JtC8o_11A8-SME_JtC8o, SME_JtC8o_11B2-SME_JtC8o	
		dc.w SME_JtC8o_11BC-SME_JtC8o, SME_JtC8o_11C4-SME_JtC8o	
		dc.w SME_JtC8o_11CC-SME_JtC8o, SME_JtC8o_11D4-SME_JtC8o	
		dc.w SME_JtC8o_11DC-SME_JtC8o, SME_JtC8o_11E4-SME_JtC8o	
		dc.w SME_JtC8o_11EE-SME_JtC8o, SME_JtC8o_11F8-SME_JtC8o	
		dc.w SME_JtC8o_1202-SME_JtC8o, SME_JtC8o_120C-SME_JtC8o	
		dc.w SME_JtC8o_1212-SME_JtC8o, SME_JtC8o_121A-SME_JtC8o	
		dc.w SME_JtC8o_1222-SME_JtC8o, SME_JtC8o_122A-SME_JtC8o	
		dc.w SME_JtC8o_1232-SME_JtC8o, SME_JtC8o_123A-SME_JtC8o	
		dc.w SME_JtC8o_1242-SME_JtC8o, SME_JtC8o_124A-SME_JtC8o	
		dc.w SME_JtC8o_1252-SME_JtC8o, SME_JtC8o_125A-SME_JtC8o	
		dc.w SME_JtC8o_1264-SME_JtC8o, SME_JtC8o_126E-SME_JtC8o	
		dc.w SME_JtC8o_1278-SME_JtC8o, SME_JtC8o_1282-SME_JtC8o	
		dc.w SME_JtC8o_128C-SME_JtC8o, SME_JtC8o_1296-SME_JtC8o	
		dc.w SME_JtC8o_12A0-SME_JtC8o, SME_JtC8o_12AA-SME_JtC8o	
		dc.w SME_JtC8o_12B4-SME_JtC8o, SME_JtC8o_12BE-SME_JtC8o	
		dc.w SME_JtC8o_12C8-SME_JtC8o, SME_JtC8o_12D2-SME_JtC8o	
		dc.w SME_JtC8o_12DC-SME_JtC8o, SME_JtC8o_12E6-SME_JtC8o	
		dc.w SME_JtC8o_12EE-SME_JtC8o, SME_JtC8o_12F6-SME_JtC8o	
		dc.w SME_JtC8o_12FC-SME_JtC8o, SME_JtC8o_1304-SME_JtC8o	
		dc.w SME_JtC8o_130C-SME_JtC8o, SME_JtC8o_1312-SME_JtC8o	
		dc.w SME_JtC8o_1318-SME_JtC8o, SME_JtC8o_131E-SME_JtC8o	
		dc.w SME_JtC8o_1326-SME_JtC8o, SME_JtC8o_132E-SME_JtC8o	
		dc.w SME_JtC8o_1332-SME_JtC8o, SME_JtC8o_133A-SME_JtC8o	
		dc.w SME_JtC8o_1340-SME_JtC8o, SME_JtC8o_1348-SME_JtC8o	
		dc.w SME_JtC8o_1350-SME_JtC8o, SME_JtC8o_1358-SME_JtC8o	
		dc.w SME_JtC8o_1360-SME_JtC8o, SME_JtC8o_136A-SME_JtC8o	
SME_JtC8o_3CC:	dc.b 0, 0	
SME_JtC8o_3CE:	dc.b 0, 3, $70, 0, $50, 8, $80, $E	
SME_JtC8o_3D6:	dc.b 0, 3, $B0, $17, $10, $23, $70, 0	
SME_JtC8o_3DE:	dc.b 0, 3, $80, $25, $30, $2E, $20, $32	
SME_JtC8o_3E6:	dc.b 0, 2, $B0, $35, $50, $41	
SME_JtC8o_3EC:	dc.b 0, 3, $B0, $47, $10, $53, $50, $41	
SME_JtC8o_3F4:	dc.b 0, 3, $B0, $55, $10, $61, $50, $41	
SME_JtC8o_3FC:	dc.b 0, 3, $80, $63, $30, $2E, $20, $32	
SME_JtC8o_404:	dc.b 0, 2, $80, $6C, $70, 0	
SME_JtC8o_40A:	dc.b 0, 4, $B0, $AF, $70, $BB, 0, $C3, $30, $C4	
SME_JtC8o_414:	dc.b 0, 4, $B0, $C8, $70, $D4, $10, $DC, $10, $DE	
SME_JtC8o_41E:	dc.b 0, 4, $80, $E0, $10, $E9, $30, $EB, $30, $EF	
SME_JtC8o_428:	dc.b 0, 4, $B0, $F3, $70, $FF, $11, 7, 1, 9	
SME_JtC8o_432:	dc.b 0, 4, $B1, $A, $71, $16, $11, $1E, $51, $20	
SME_JtC8o_43C:	dc.b 0, 4, $F1, $26, $51, $36, $21, $3C, $31, $3F	
SME_JtC8o_446:	dc.b 0, 4, $81, $43, $71, $4C, $11, $54, 1, $56	
SME_JtC8o_450:	dc.b 0, 4, $81, $57, $21, $60, $31, $63, $31, $67	
SME_JtC8o_45A:	dc.b 0, 3, $51, $EF, $81, $F5, $71, $FE	
SME_JtC8o_462:	dc.b 0, 3, $B2, 6, $12, $12, $72, $14	
SME_JtC8o_46A:	dc.b 0, 3, $82, $1C, $52, $25, 2, $2B	
SME_JtC8o_472:	dc.b 0, 2, $B2, $2C, $72, $38	
SME_JtC8o_478:	dc.b 0, 3, $12, $40, $B2, $42, $72, $38	
SME_JtC8o_480:	dc.b 0, 4, $12, $4E, $B2, $50, $52, $5C, 2, $62	
SME_JtC8o_48A:	dc.b 0, 3, $82, $63, $52, $25, 2, $2B	
SME_JtC8o_492:	dc.b 0, 2, $82, $6C, $71, $FE	
SME_JtC8o_498:	dc.b 0, 4, $B2, $E3, $52, $EF, $32, $F5, $22, $F9	
SME_JtC8o_4A2:	dc.b 0, 4, $B2, $FC, $13, 8, $33, $A, $53, $E	
SME_JtC8o_4AC:	dc.b 0, 4, $83, $14, $33, $1D, $33, $21, $13, $25	
SME_JtC8o_4B6:	dc.b 0, 4, $B3, $27, $33, $33, $33, $37, $23, $3B	
SME_JtC8o_4C0:	dc.b 0, 4, $B3, $3E, $53, $4A, $73, $50, 3, $58	
SME_JtC8o_4CA:	dc.b 0, 4, $F3, $59, $23, $69, $13, $6C, $73, $6E	
SME_JtC8o_4D4:	dc.b 0, 4, $83, $76, $33, $7F, $33, $83, $23, $87	
SME_JtC8o_4DE:	dc.b 0, 4, $83, $8A, $33, $93, $33, $97, $23, $9B	
SME_JtC8o_4E8:	dc.b 0, 2, $24, $21, $F4, $24	
SME_JtC8o_4EE:	dc.b 0, 2, $24, $34, $F4, $37	
SME_JtC8o_4F4:	dc.b 0, 2, $24, $21, $F4, $47	
SME_JtC8o_4FA:	dc.b 0, 2, $24, $34, $F4, $57	
SME_JtC8o_500:	dc.b 0, 4, $B4, $8B, 4, $97, $34, $98, $24, $9C	
SME_JtC8o_50A:	dc.b 0, 2, $24, $9F, $F4, $A2	
SME_JtC8o_510:	dc.b 0, 4, $B4, $B2, 4, $BE, $34, $BF, $24, $C3	
SME_JtC8o_51A:	dc.b 0, 2, $24, $9F, $F4, $C6	
SME_JtC8o_520:	dc.b 0, 2, $25, 4, $F5, 7	
SME_JtC8o_526:	dc.b 0, 2, $25, $17, $F5, $1A	
SME_JtC8o_52C:	dc.b 0, 2, $25, 4, $F5, $2A	
SME_JtC8o_532:	dc.b 0, 2, $25, $17, $F5, $3A	
SME_JtC8o_538:	dc.b 0, 3, $15, $70, $B5, $72, $55, $7E	
SME_JtC8o_540:	dc.b 0, 2, $25, $84, $F5, $87	
SME_JtC8o_546:	dc.b 0, 3, $15, $97, $B5, $99, $55, $A5	
SME_JtC8o_54E:	dc.b 0, 2, $25, $84, $F5, $AB	
SME_JtC8o_554:	dc.b 0, 3, $25, $E6, $B5, $E9, $25, $F5	
SME_JtC8o_55C:	dc.b 0, 4, $55, $F8, $35, $FE, $26, 2, $16, 5	
SME_JtC8o_566:	dc.b 0, 4, $16, 7, $26, 9, $36, $C, $16, $10	
SME_JtC8o_570:	dc.b 0, 4, $26, $12, $36, $15, $26, $19, $36, $1C	
SME_JtC8o_57A:	dc.b 0, 4, $16, $20, $26, $22, $36, $25, $26, $29	
SME_JtC8o_584:	dc.b 0, 3, $16, $2C, $76, $2E, $56, $36	
SME_JtC8o_58C:	dc.b 0, 2, $F6, $3C, $26, $4C	
SME_JtC8o_592:	dc.b 0, 2, $B6, $4F, 6, $5B	
SME_JtC8o_598:	dc.b 0, 3, $16, $5C, $76, $5E, $26, $66	
SME_JtC8o_5A0:	dc.b 0, 4, $26, $69, $36, $6C, $26, $70, $16, $73	
SME_JtC8o_5AA:	dc.b 0, 2, $86, $75, $36, $7E	
SME_JtC8o_5B0:	dc.b 0, 2, $B6, $82, $26, $8E	
SME_JtC8o_5B6:	dc.b 0, 3, 6, $91, $B6, $92, $26, $9E	
SME_JtC8o_5BE:	dc.b 0, 1, $B6, $A1	
SME_JtC8o_5C2:	dc.b 0, 3, $26, $AD, $76, $B0, $16, $B8	
SME_JtC8o_5CA:	dc.b 0, 4, $26, $BA, $36, $BD, $26, $C1, $16, $C4	
SME_JtC8o_5D4:	dc.b 0, 4, $56, $C6, $36, $CC, $26, $D0, $16, $D3	
SME_JtC8o_5DE:	dc.b 0, 4, $56, $D5, $26, $DB, $36, $DE, $26, $E2	
SME_JtC8o_5E8:	dc.b 0, 2, $B6, $E5, $26, $F1	
SME_JtC8o_5EE:	dc.b 0, 1, $B6, $F4	
SME_JtC8o_5F2:	dc.b 0, 2, $77, 0, $27, 8	
SME_JtC8o_5F8:	dc.b 0, 1, $B7, $B	
SME_JtC8o_5FC:	dc.b 0, 2, $17, $17, $B7, $19	
SME_JtC8o_602:	dc.b 0, 3, $27, $25, $77, $28, $57, $30	
SME_JtC8o_60A:	dc.b 0, 4, $87, $36, $37, $3F, $37, $43, $27, $47	
SME_JtC8o_614:	dc.b 0, 4, $27, $4A, $77, $4D, $27, $55, $37, $58	
SME_JtC8o_61E:	dc.b 0, 3, $57, $5C, $37, $62, $27, $66	
SME_JtC8o_626:	dc.b 0, 3, $27, $69, $77, $6C, $27, $74	
SME_JtC8o_62E:	dc.b 0, 3, $17, $77, $77, $79, $17, $81	
SME_JtC8o_636:	dc.b 0, 1, $F7, $83	
SME_JtC8o_63A:	dc.b 0, 4, 7, $93, $77, $94, 7, $9C, $77, $9D	
SME_JtC8o_644:	dc.b 0, 4, $17, $A5, $27, $A7, $87, $AA, $37, $B3	
SME_JtC8o_64E:	dc.b 0, 2, $17, $B7, $B7, $B9	
SME_JtC8o_654:	dc.b 0, 3, $17, $C5, $77, $C7, $17, $CF	
SME_JtC8o_65C:	dc.b 0, 3, 7, $D1, $87, $D2, $17, $DB	
SME_JtC8o_664:	dc.b 0, 2, $F7, $DD, 7, $ED	
SME_JtC8o_66A:	dc.b 0, 4, $28, 0, $78, 3, $28, $B, $28, $E	
SME_JtC8o_674:	dc.b 0, 3, $28, $11, $78, $14, $58, $1C	
SME_JtC8o_67C:	dc.b 0, 2, $B8, $22, $38, $2E	
SME_JtC8o_682:	dc.b 0, 2, $B8, $32, $28, $3E	
SME_JtC8o_688:	dc.b 0, 4, $28, $41, $38, $44, $28, $48, $78, $4B	
SME_JtC8o_692:	dc.b 0, 3, $88, $53, $38, $5C, $28, $60	
SME_JtC8o_69A:	dc.b 0, 2, $B8, $63, $28, $6F	
SME_JtC8o_6A0:	dc.b 0, 3, $88, $72, $58, $7B, 8, $81	
SME_JtC8o_6A8:	dc.b 0, 1, $B8, $82	
SME_JtC8o_6AC:	dc.b 0, 2, $B8, $8E, $28, $9A	
SME_JtC8o_6B2:	dc.b 0, 3, $38, $9D, $B8, $A1, 8, $AD	
SME_JtC8o_6BA:	dc.b 0, 1, $B8, $AE	
SME_JtC8o_6BE:	dc.b 0, 2, 8, $BA, $B8, $BB	
SME_JtC8o_6C4:	dc.b 0, 2, $88, $C7, $58, $D0	
SME_JtC8o_6CA:	dc.b 0, 3, $88, $D6, $38, $DF, $28, $E3	
SME_JtC8o_6D2:	dc.b 0, 1, $B8, $E6	
SME_JtC8o_6D6:	dc.b 0, 2, $B8, $F2, $28, $FE	
SME_JtC8o_6DC:	dc.b 0, 3, $29, 1, $19, 4, $B9, 6	
SME_JtC8o_6E4:	dc.b 0, 1, $B9, $12	
SME_JtC8o_6E8:	dc.b 0, 2, 9, $1E, $B9, $1F	
SME_JtC8o_6EE:	dc.b 0, 2, $B9, $2B, $29, $37	
SME_JtC8o_6F4:	dc.b 0, 4, $19, $3A, $39, $3C, $59, $40, $19, $46	
SME_JtC8o_6FE:	dc.b 0, 3, 9, $48, $79, $49, $29, $51	
SME_JtC8o_706:	dc.b 0, 3, 9, $54, $79, $55, $29, $5D	
SME_JtC8o_70E:	dc.b 0, 2, $59, $60, $B9, $66	
SME_JtC8o_714:	dc.b 0, 3, 9, $72, $89, $73, $39, $7C	
SME_JtC8o_71C:	dc.b 0, 3, $19, $80, $59, $82, $39, $88	
SME_JtC8o_724:	dc.b 0, 3, $59, $8C, $39, $92, $19, $96	
SME_JtC8o_72C:	dc.b 0, 2, 9, $98, $B9, $99	
SME_JtC8o_732:	dc.b 0, 4, $19, $A5, 9, $A7, 9, $A8, $B9, $A9	
SME_JtC8o_73C:	dc.b 0, 3, $19, $B5, $B9, $B7, $29, $C3	
SME_JtC8o_744:	dc.b 0, 3, $19, $C6, $79, $C8, $29, $D0	
SME_JtC8o_74C:	dc.b 0, 2, 9, $D3, $89, $D4	
SME_JtC8o_752:	dc.b 0, 3, $29, $DD, $79, $E0, $29, $E8	
SME_JtC8o_75A:	dc.b 0, 1, $B9, $EB	
SME_JtC8o_75E:	dc.b 0, 4, $39, $F7, $29, $FB, $79, $FE, $7A, 6	
SME_JtC8o_768:	dc.b 0, 4, $1A, $E, $BA, $10, $5A, $1C, $2A, $22	
SME_JtC8o_772:	dc.b 0, 3, $2A, $25, $BA, $28, $7A, $34	
SME_JtC8o_77A:	dc.b 0, 4, $2A, $3C, $1A, $3F, $1A, $41, $FA, $43	
SME_JtC8o_784:	dc.b 0, 4, $39, $F7, $29, $FB, $79, $FE, $7A, 6	
SME_JtC8o_78E:	dc.b 0, 4, $1A, $E, $BA, $10, $5A, $1C, $2A, $22	
SME_JtC8o_798:	dc.b 0, 3, $2A, $25, $BA, $28, $7A, $34	
SME_JtC8o_7A0:	dc.b 0, 4, $2A, $3C, $1A, $3F, $1A, $41, $FA, $43	
SME_JtC8o_7AA:	dc.b 0, 4, $BA, $53, $A, $5F, $1A, $60, $5A, $62	
SME_JtC8o_7B4:	dc.b 0, 4, $3A, $68, $7A, $6C, $3A, $74, $2A, $78	
SME_JtC8o_7BE:	dc.b 0, 3, $2A, $7B, $3A, $7E, $BA, $82	
SME_JtC8o_7C6:	dc.b 0, 4, $A, $8E, $7A, $8F, $2A, $97, $5A, $9A	
SME_JtC8o_7D0:	dc.b 0, 3, $BA, $A0, $5A, $AC, $1A, $B2	
SME_JtC8o_7D8:	dc.b 0, 2, $BA, $B4, $5A, $C0	
SME_JtC8o_7DE:	dc.b 0, 3, $2A, $C6, $7A, $C9, $2A, $D1	
SME_JtC8o_7E6:	dc.b 0, 2, $BA, $D4, $2A, $E0	
SME_JtC8o_7EC:	dc.b 0, 2, $BA, $E3, $2A, $E0	
SME_JtC8o_7F2:	dc.b 0, 2, $BA, $EF, $2A, $E0	
SME_JtC8o_7F8:	dc.b 0, 2, $BA, $FB, $2A, $E0	
SME_JtC8o_7FE:	dc.b 0, 2, $BB, 7, $2A, $E0	
SME_JtC8o_804:	dc.b 0, 3, $BB, $13, $2B, $1F, $2B, $22	
SME_JtC8o_80C:	dc.b 0, 3, $BB, $25, $2B, $1F, $2B, $22	
SME_JtC8o_814:	dc.b 0, 2, $B7, $EF, $37, $FB	
SME_JtC8o_81A:	dc.b 0, 3, $3B, $31, $3B, $35, $8B, $39	
SME_JtC8o_822:	dc.b 0, 4, $5B, $42, $3B, $48, $2B, $4C, $5B, $4F	
SME_JtC8o_82C:	dc.b 0, 4, $5B, $55, $3B, $5B, $2B, $5F, $7B, $62	
SME_JtC8o_836:	dc.b 0, 4, $2B, $6A, $BB, $6D, $B, $79, $1B, $7A	
SME_JtC8o_840:	dc.b 0, 2, $2B, $6A, $BB, $7C	
SME_JtC8o_846:	dc.b 0, 5, $7B, $88, $1B, $93, $5B, $95, $3B, $9B, $2B, $9F	
SME_JtC8o_852:	dc.b 0, 5, $2B, $90, $1B, $93, $5B, $95, $3B, $9B, $2B, $9F	
SME_JtC8o_85E:	dc.b 0, 1, $FB, $A2	
SME_JtC8o_862:	dc.b 0, 1, $FB, $B2	
SME_JtC8o_866:	dc.b 0, 1, $FB, $C2	
SME_JtC8o_86A:	dc.b 0, 1, $FB, $D2	
SME_JtC8o_86E:	dc.b 0, 1, $FB, $E2	
SME_JtC8o_872:	dc.b 0, 2, $B, $F2, $BB, $F3	
SME_JtC8o_878:	dc.b 0, 2, $B, $FF, $BC, 0	
SME_JtC8o_87E:	dc.b 0, 4, $8C, $C, $1C, $15, $3C, $17, $2C, $1B	
SME_JtC8o_888:	dc.b 0, 4, $8C, $C, $1C, $1E, $3C, $20, $2C, $24	
SME_JtC8o_892:	dc.b 0, 4, $2C, $27, $7C, $2A, $2C, $32, $1C, $35	
SME_JtC8o_89C:	dc.b 0, 3, $2C, $37, $BC, $3A, $2C, $46	
SME_JtC8o_8A4:	dc.b 0, 3, $7C, $49, $C, $51, $8C, $52	
SME_JtC8o_8AC:	dc.b 0, 4, $C, $5B, $BC, $5C, $C, $68, $5C, $69	
SME_JtC8o_8B6:	dc.b 0, 3, $C, $6F, $FC, $70, $2C, $80	
SME_JtC8o_8BE:	dc.b 0, 4, $2C, $83, $BC, $86, $1C, $92, $2C, $94	
SME_JtC8o_8C8:	dc.b 0, 3, $FC, $97, $C, $A7, $2C, $A8	
SME_JtC8o_8D0:	dc.b 0, 4, $2C, $83, $BC, $AB, $1C, $B7, $2C, $B9	
SME_JtC8o_8DA:	dc.b 0, 3, $2C, $BC, $FC, $BF, $C, $CF	
SME_JtC8o_8E2:	dc.b 0, 3, $2C, $D0, $FC, $D3, $C, $CF	
SME_JtC8o_8EA:	dc.b 0, 3, $1C, $E3, $FC, $E5, $C, $CF	
SME_JtC8o_8F2:	dc.b 0, 3, $BC, $F5, $D, 1, $5D, 2	
SME_JtC8o_8FA:	dc.b 0, 3, $BD, 8, $D, $14, $5D, $15	
SME_JtC8o_902:	dc.b 0, 4, $FD, $1B, $D, $2B, $1D, $2C, $1D, $2E	
SME_JtC8o_90C:	dc.b 0, 4, $2D, $30, $7D, $33, $1D, $3B, $2D, $3D	
SME_JtC8o_916:	dc.b 0, 4, $2D, $30, $7D, $40, $1D, $3B, $2D, $3D	
SME_JtC8o_920:	dc.b 0, 4, $2D, $48, $3D, $4B, $7D, $4F, $2D, $3D	
SME_JtC8o_92A:	dc.b 0, 2, $7D, $76, $8D, $7E	
SME_JtC8o_930:	dc.b 0, 3, $2D, $87, $7D, $8A, $5D, $92	
SME_JtC8o_938:	dc.b 0, 3, $2D, $87, $7D, $98, $5D, $92	
SME_JtC8o_940:	dc.b 0, 3, $2D, $87, $7D, $A0, $5D, $92	
SME_JtC8o_948:	dc.b 0, 3, $BD, $A8, $2D, $B4, $3D, $B7	
SME_JtC8o_950:	dc.b 0, 3, $BD, $BB, $2D, $C7, $3D, $CA	
SME_JtC8o_958:	dc.b 0, 3, $1D, $CE, $8D, $D0, $3D, $D9	
SME_JtC8o_960:	dc.b 0, 3, $1D, $CE, $8D, $DD, $1D, $E6	
SME_JtC8o_968:	dc.b 0, 3, $1D, $CE, $5D, $E8, $7D, $EE	
SME_JtC8o_970:	dc.b 0, 3, $1D, $CE, $8D, $F6, $1D, $FF	
SME_JtC8o_978:	dc.b 0, 4, $8E, 1, $28, $11, $3E, $A, 8, $15	
SME_JtC8o_982:	dc.b 0, 3, $2E, $E, $7E, $11, $5E, $19	
SME_JtC8o_98A:	dc.b 0, 4, $2E, $1F, $7E, $22, $2E, $2A, $2E, $2D	
SME_JtC8o_994:	dc.b 0, 4, $2D, $87, $7E, $30, $2E, $2A, $2E, $2D	
SME_JtC8o_99E:	dc.b 0, 4, $2D, $87, $7E, $30, $2E, $2A, $2E, $38	
SME_JtC8o_9A8:	dc.b 0, 4, $2D, $87, $7E, $3B, $2E, $2A, $2E, $2D	
SME_JtC8o_9B2:	dc.b 0, 4, $2D, $87, $7E, $3B, $2E, $2A, $2E, $38	
SME_JtC8o_9BC:	dc.b 0, 2, $BE, $43, $3E, $4F	
SME_JtC8o_9C2:	dc.b 0, 4, $2E, $53, $7E, $56, $2E, $5E, $3E, $4F	
SME_JtC8o_9CC:	dc.b 0, 4, $2E, $61, $7E, $64, $2E, $6C, $2E, $6F	
SME_JtC8o_9D6:	dc.b 0, 4, $2E, $72, $3E, $75, $3E, $79, $5E, $7D	
SME_JtC8o_9E0:	dc.b 0, 3, $BE, $83, $2E, $8F, $2E, $6F	
SME_JtC8o_9E8:	dc.b 0, 4, $2E, $92, $7E, $95, $2E, $9D, $2E, $A0	
SME_JtC8o_9F2:	dc.b 0, 4, $2E, $A3, $3E, $A6, $5E, $AA, $2E, $6F	
SME_JtC8o_9FC:	dc.b 0, 3, $8E, $B0, $1E, $B9, $2E, $BB	
SME_JtC8o_A04:	dc.b 0, 3, $5E, $BE, $3E, $C4, $E, $C8	
SME_JtC8o_A0C:	dc.b 0, 2, $5E, $C9, $3E, $CF	
SME_JtC8o_A12:	dc.b 0, 3, $5E, $BE, $3E, $C4, $E, $C8	
SME_JtC8o_A1A:	dc.b 0, 3, $8E, $B0, $1E, $B9, $2E, $BB	
SME_JtC8o_A22:	dc.b 0, 2, $BE, $D3, $E, $DF	
SME_JtC8o_A28:	dc.b 0, 2, $5E, $E0, $3E, $E6	
SME_JtC8o_A2E:	dc.b 0, 2, $BE, $D3, $E, $DF	
SME_JtC8o_A34:	dc.b 0, 3, $BB, $13, $2B, $1F, $2B, $22	
SME_JtC8o_A3C:	dc.b 0, 3, $BB, $25, $2B, $1F, $2B, $22	
SME_JtC8o_A44:	dc.b 0, 1, $BF, $1C	
SME_JtC8o_A48:	dc.b 0, 3, $5F, $28, $7F, $2E, $5F, $36	
SME_JtC8o_A50:	dc.b 0, 2, $FF, $3C, $5F, $4C	
SME_JtC8o_A56:	dc.b 0, 3, $8F, $52, $3F, $5B, $5F, $5F	
SME_JtC8o_A5E:	dc.b 0, 3, $8F, $65, $3F, $5B, $5F, $5F	
SME_JtC8o_A66:	dc.b 0, 3, $BB, $13, $2B, $1F, $2B, $22	
SME_JtC8o_A6E:	dc.b 0, 3, $BB, $25, $2B, $1F, $2B, $22	
SME_JtC8o_A76:	dc.b 0, 4, $BD, $57, $D, $63, $3D, $64, $2D, $68	
SME_JtC8o_A80:	dc.b 0, 3, $50, 0, $70, 6, $10, $E	
SME_JtC8o_A88:	dc.b 0, 3, $50, 0, $70, 6, $10, $E	
SME_JtC8o_A90:	dc.b 0, 3, $50, 0, $70, 6, $10, $E	
SME_JtC8o_A98:	dc.b 0, 3, $50, 0, $70, 6, $10, $E	
SME_JtC8o_AA0:	dc.b 0, 3, $50, 0, $70, 6, $10, $E	
SME_JtC8o_AA8:	dc.b 0, 3, $50, 0, $70, 6, $10, $E	
SME_JtC8o_AB0:	dc.b 0, 1, $B0, $10	
SME_JtC8o_AB4:	dc.b 0, 2, $B0, $1C, $20, $28	
SME_JtC8o_ABA:	dc.b 0, 4, $50, $2B, $30, $31, $20, $35, $10, $38	
SME_JtC8o_AC4:	dc.b 0, 4, $B0, $3A, $30, $46, $20, $4A, $10, $4D	
SME_JtC8o_ACE:	dc.b 0, 4, $50, $4F, $70, $55, $10, $5D, $20, $5F	
SME_JtC8o_AD8:	dc.b 0, 4, $50, $62, $30, $68, $20, $6C, $50, $6F	
SME_JtC8o_AE2:	dc.b 0, 3, $B0, $75, $10, $81, $20, $83	
SME_JtC8o_AEA:	dc.b 0, 2, $B0, $86, $50, $92	
SME_JtC8o_AF0:	dc.b 0, 4, $50, $98, $30, $9E, $20, $A2, $50, $A5	
SME_JtC8o_AFA:	dc.b 0, 3, $30, $AB, $80, $AF, $50, $B8	
SME_JtC8o_B02:	dc.b 0, 2, $B0, $BE, $50, $CA	
SME_JtC8o_B08:	dc.b 0, 4, $50, $D0, $30, $D6, $20, $DA, $70, $DD	
SME_JtC8o_B12:	dc.b 0, 3, $10, $E5, $F0, $E7, $50, $F7	
SME_JtC8o_B1A:	dc.b 0, 3, $50, $FD, $71, 3, $51, $B	
SME_JtC8o_B22:	dc.b 0, 4, $11, $11, $71, $13, $31, $1B, $31, $1F	
SME_JtC8o_B2C:	dc.b 0, 2, $F1, $23, $21, $33	
SME_JtC8o_B32:	dc.b 0, 3, $B1, $36, $21, $42, $11, $45	
SME_JtC8o_B3A:	dc.b 0, 2, $B1, $47, $21, $53	
SME_JtC8o_B40:	dc.b 0, 2, $B1, $56, 1, $62	
SME_JtC8o_B46:	dc.b 0, 2, $B1, $63, $21, $6F	
SME_JtC8o_B4C:	dc.b 0, 4, $71, $72, $21, $7A, $31, $7D, $21, $81	
SME_JtC8o_B56:	dc.b 0, 3, $21, $84, $B1, $87, $21, $93	
SME_JtC8o_B5E:	dc.b 0, 2, $F1, $96, $21, $A6	
SME_JtC8o_B64:	dc.b 0, 2, $B1, $A9, $11, $B5	
SME_JtC8o_B6A:	dc.b 0, 3, $81, $B7, $11, $C0, $21, $C2	
SME_JtC8o_B72:	dc.b 0, 2, $B1, $C5, $11, $D1	
SME_JtC8o_B78:	dc.b 0, 2, $B1, $D3, $51, $DF	
SME_JtC8o_B7E:	dc.b 0, 7, $31, $E5, 1, $E9, $31, $EA, 1, $EE, $31, $EF, 1, $F3, $31, $F4	
SME_JtC8o_B8E:	dc.b 0, 7, $31, $F8, 1, $FC, $31, $FD, 2, 1, $32, 2, 2, 6, $32, 7	
SME_JtC8o_B9E:	dc.b 0, 7, $31, $E5, 1, $E9, $31, $EA, 1, $EE, $31, $EF, 1, $F3, $31, $F4	
SME_JtC8o_BAE:	dc.b 0, 7, $31, $F8, 1, $FC, $31, $FD, 2, 1, $32, 2, 2, 6, $32, 7	
SME_JtC8o_BBE:	dc.b 0, 6, $22, $B, 2, $E, $32, $F, $32, $13, $32, $17, $32, $1B	
SME_JtC8o_BCC:	dc.b 0, 5, $22, $1F, $32, $22, $32, $26, $32, $2A, $32, $2E	
SME_JtC8o_BD8:	dc.b 0, 6, $22, $B, 2, $E, $32, $F, $32, $13, $32, $17, $32, $1B	
SME_JtC8o_BE6:	dc.b 0, 5, $22, $1F, $32, $22, $32, $26, $32, $2A, $32, $2E	
SME_JtC8o_BF2:	dc.b 0, 5, $22, $32, $32, $35, $32, $39, $32, $3D, $32, $41	
SME_JtC8o_BFE:	dc.b 0, 5, $22, $45, $32, $48, $32, $4C, $32, $50, $32, $54	
SME_JtC8o_C0A:	dc.b 0, 5, $22, $32, $32, $35, $32, $39, $32, $3D, $32, $41	
SME_JtC8o_C16:	dc.b 0, 5, $22, $45, $32, $48, $32, $4C, $32, $50, $32, $54	
SME_JtC8o_C22:	dc.b 0, 9, $32, $58, 2, $5C, $32, $5D, 2, $61, $32, $62, 2, $66, $32, $67, 2, $6B, $12, $6C	
SME_JtC8o_C36:	dc.b 0, 7, $32, $6E, $32, $72, 2, $76, $32, $77, 2, $7B, $32, $7C, $22, $80	
SME_JtC8o_C46:	dc.b 0, 9, $32, $58, 2, $5C, $32, $5D, 2, $61, $32, $62, 2, $66, $32, $67, 2, $6B, $12, $6C	
SME_JtC8o_C5A:	dc.b 0, 7, $32, $6E, $32, $72, 2, $76, $32, $77, 2, $7B, $32, $7C, $22, $80	
SME_JtC8o_C6A:	dc.b 0, 0	
SME_JtC8o_C6C:	dc.b 0, 3, $80, $E, $50, 8, $B0, $75	
SME_JtC8o_C74:	dc.b 0, 3, $B0, $17, $10, $23, $B0, $81	
SME_JtC8o_C7C:	dc.b 0, 3, $70, $8D, $80, $25, $20, $95	
SME_JtC8o_C84:	dc.b 0, 3, $B0, $35, $50, $98, $30, $9E	
SME_JtC8o_C8C:	dc.b 0, 4, $B0, $47, $10, $53, $30, $A2, $50, $A6	
SME_JtC8o_C96:	dc.b 0, 4, $B0, $55, $10, $61, $30, $9E, $50, $98	
SME_JtC8o_CA0:	dc.b 0, 3, $80, $63, $20, $AC, $70, $8D	
SME_JtC8o_CA8:	dc.b 0, 2, $80, $6C, $B0, $81	
SME_JtC8o_CAE:	dc.b 0, 4, $B0, $AF, $11, $6B, $B1, $6D, $51, $79	
SME_JtC8o_CB8:	dc.b 0, 4, $B0, $C8, $11, $7F, $81, $81, $71, $8A	
SME_JtC8o_CC2:	dc.b 0, 4, $80, $E0, $71, $92, 1, $9A, $11, $9B	
SME_JtC8o_CCC:	dc.b 0, 4, $B0, $F3, $B1, $9D, 1, $A9, $31, $AA	
SME_JtC8o_CD6:	dc.b 0, 4, $B1, $A, $81, $AE, $51, $B7, $51, $BD	
SME_JtC8o_CE0:	dc.b 0, 4, $F1, $26, $71, $C3, $21, $CB, $31, $CE	
SME_JtC8o_CEA:	dc.b 0, 4, $81, $43, $51, $D2, $81, $D8, $11, $E1	
SME_JtC8o_CF4:	dc.b 0, 4, $81, $57, $71, $E3, 1, $EB, $21, $EC	
SME_JtC8o_CFE:	dc.b 0, 3, $51, $EF, $81, $F5, $B2, $75	
SME_JtC8o_D06:	dc.b 0, 3, $12, $81, $B2, $83, $B2, $8F	
SME_JtC8o_D0E:	dc.b 0, 3, $82, $9B, $82, $A4, $12, $AD	
SME_JtC8o_D16:	dc.b 0, 3, $B2, $2C, $82, $AF, 2, $B8	
SME_JtC8o_D1E:	dc.b 0, 4, $B2, $42, $12, $40, $82, $B9, 2, $C2	
SME_JtC8o_D28:	dc.b 0, 4, $B2, $C3, $12, $CF, $82, $AF, 2, $B8	
SME_JtC8o_D32:	dc.b 0, 3, $82, $D1, $82, $DA, $12, $AD	
SME_JtC8o_D3A:	dc.b 0, 2, $82, $6C, $B2, $8F	
SME_JtC8o_D40:	dc.b 0, 4, $B2, $E3, $53, $9E, $B3, $A4, 3, $B0	
SME_JtC8o_D4A:	dc.b 0, 4, $B2, $FC, $73, $B1, $73, $B9, $13, $C1	
SME_JtC8o_D54:	dc.b 0, 4, $83, $14, 3, $C3, $13, $C4, $83, $C6	
SME_JtC8o_D5E:	dc.b 0, 4, $B3, $27, $33, $CF, $B3, $D3, 3, $3A	
SME_JtC8o_D68:	dc.b 0, 4, $B3, $3E, $53, $DF, $83, $E5, $53, $EE	
SME_JtC8o_D72:	dc.b 0, 4, $F3, $59, $33, $F4, 3, $F8, $B3, $F9	
SME_JtC8o_D7C:	dc.b 0, 4, $83, $76, $34, 5, $B4, 9, 3, $86	
SME_JtC8o_D86:	dc.b 0, 4, $83, $8A, $34, $15, $54, $19, $14, $1F	
SME_JtC8o_D90:	dc.b 0, 4, $74, $67, $24, $6F, $54, $72, $14, $78	
SME_JtC8o_D9A:	dc.b 0, 4, $74, $7A, $24, $82, $54, $85, $14, $78	
SME_JtC8o_DA4:	dc.b 0, 4, $74, $67, $24, $6F, $54, $72, $14, $78	
SME_JtC8o_DAE:	dc.b 0, 4, $74, $7A, $24, $82, $54, $85, $14, $78	
SME_JtC8o_DB8:	dc.b 0, 4, $14, $D6, $74, $D8, 4, $E0, $B4, $E1	
SME_JtC8o_DC2:	dc.b 0, 4, $14, $ED, $74, $EF, 4, $F7, $B4, $F8	
SME_JtC8o_DCC:	dc.b 0, 4, $14, $D6, $74, $D8, 4, $E0, $B4, $E1	
SME_JtC8o_DD6:	dc.b 0, 4, $14, $ED, $74, $EF, 4, $F7, $B4, $F8	
SME_JtC8o_DE0:	dc.b 0, 4, $B5, $4A, $15, $56, 5, $58, $35, $59	
SME_JtC8o_DEA:	dc.b 0, 4, $B5, $5D, $15, $69, 5, $6B, $35, $6C	
SME_JtC8o_DF4:	dc.b 0, 4, $B5, $4A, $15, $56, 5, $58, $35, $59	
SME_JtC8o_DFE:	dc.b 0, 4, $B5, $5D, $15, $69, 5, $6B, $35, $6C	
SME_JtC8o_E08:	dc.b 0, 4, $35, $BB, $75, $BF, $55, $C7, $35, $CD	
SME_JtC8o_E12:	dc.b 0, 4, $25, $D1, $75, $D4, $55, $DC, $35, $E2	
SME_JtC8o_E1C:	dc.b 0, 4, $35, $BB, $75, $BF, $55, $C7, $35, $CD	
SME_JtC8o_E26:	dc.b 0, 4, $25, $D1, $75, $D4, $55, $DC, $35, $E2	
SME_JtC8o_E30:	dc.b 0, 3, $25, $E6, $B5, $E9, $25, $F5	
SME_JtC8o_E38:	dc.b 0, 4, $55, $F8, $35, $FE, $26, 2, $16, 5	
SME_JtC8o_E42:	dc.b 0, 4, $16, 7, $26, 9, $36, $C, $16, $10	
SME_JtC8o_E4C:	dc.b 0, 4, $26, $12, $36, $15, $26, $19, $36, $1C	
SME_JtC8o_E56:	dc.b 0, 4, $16, $20, $26, $22, $36, $25, $26, $29	
SME_JtC8o_E60:	dc.b 0, 3, $16, $2C, $76, $2E, $56, $36	
SME_JtC8o_E68:	dc.b 0, 2, $F6, $3C, $26, $4C	
SME_JtC8o_E6E:	dc.b 0, 2, $B6, $4F, 6, $5B	
SME_JtC8o_E74:	dc.b 0, 3, $16, $5C, $76, $5E, $26, $66	
SME_JtC8o_E7C:	dc.b 0, 4, $26, $69, $36, $6C, $26, $70, $16, $73	
SME_JtC8o_E86:	dc.b 0, 2, $86, $75, $36, $7E	
SME_JtC8o_E8C:	dc.b 0, 2, $B6, $82, $26, $8E	
SME_JtC8o_E92:	dc.b 0, 3, 6, $91, $B6, $92, $26, $9E	
SME_JtC8o_E9A:	dc.b 0, 1, $B6, $A1	
SME_JtC8o_E9E:	dc.b 0, 3, $26, $AD, $76, $B0, $16, $B8	
SME_JtC8o_EA6:	dc.b 0, 4, $26, $BA, $36, $BD, $26, $C1, $16, $C4	
SME_JtC8o_EB0:	dc.b 0, 4, $56, $C6, $36, $CC, $26, $D0, $16, $D3	
SME_JtC8o_EBA:	dc.b 0, 4, $56, $D5, $26, $DB, $36, $DE, $26, $E2	
SME_JtC8o_EC4:	dc.b 0, 2, $B6, $E5, $26, $F1	
SME_JtC8o_ECA:	dc.b 0, 1, $B6, $F4	
SME_JtC8o_ECE:	dc.b 0, 2, $77, 0, $27, 8	
SME_JtC8o_ED4:	dc.b 0, 1, $B7, $B	
SME_JtC8o_ED8:	dc.b 0, 2, $17, $17, $B7, $19	
SME_JtC8o_EDE:	dc.b 0, 3, $27, $25, $77, $28, $57, $30	
SME_JtC8o_EE6:	dc.b 0, 4, $87, $36, $37, $3F, $37, $43, $27, $47	
SME_JtC8o_EF0:	dc.b 0, 4, $27, $4A, $77, $4D, $27, $55, $37, $58	
SME_JtC8o_EFA:	dc.b 0, 3, $57, $5C, $37, $62, $27, $66	
SME_JtC8o_F02:	dc.b 0, 3, $27, $69, $77, $6C, $27, $74	
SME_JtC8o_F0A:	dc.b 0, 3, $17, $77, $77, $79, $17, $81	
SME_JtC8o_F12:	dc.b 0, 1, $F7, $83	
SME_JtC8o_F16:	dc.b 0, 4, 7, $93, $77, $94, 7, $9C, $77, $9D	
SME_JtC8o_F20:	dc.b 0, 4, $17, $A5, $27, $A7, $87, $AA, $37, $B3	
SME_JtC8o_F2A:	dc.b 0, 2, $17, $B7, $B7, $B9	
SME_JtC8o_F30:	dc.b 0, 3, $17, $C5, $77, $C7, $17, $CF	
SME_JtC8o_F38:	dc.b 0, 3, 7, $D1, $87, $D2, $17, $DB	
SME_JtC8o_F40:	dc.b 0, 2, $F7, $DD, 7, $ED	
SME_JtC8o_F46:	dc.b 0, 4, $28, 0, $78, 3, $28, $B, $28, $E	
SME_JtC8o_F50:	dc.b 0, 3, $28, $11, $78, $14, $58, $1C	
SME_JtC8o_F58:	dc.b 0, 2, $B8, $22, $38, $2E	
SME_JtC8o_F5E:	dc.b 0, 2, $B8, $32, $28, $3E	
SME_JtC8o_F64:	dc.b 0, 4, $28, $41, $38, $44, $28, $48, $78, $4B	
SME_JtC8o_F6E:	dc.b 0, 3, $88, $53, $38, $5C, $28, $60	
SME_JtC8o_F76:	dc.b 0, 2, $B8, $63, $28, $6F	
SME_JtC8o_F7C:	dc.b 0, 3, $88, $72, $58, $7B, 8, $81	
SME_JtC8o_F84:	dc.b 0, 1, $B8, $82	
SME_JtC8o_F88:	dc.b 0, 2, $B8, $8E, $28, $9A	
SME_JtC8o_F8E:	dc.b 0, 3, $38, $9D, $B8, $A1, 8, $AD	
SME_JtC8o_F96:	dc.b 0, 1, $B8, $AE	
SME_JtC8o_F9A:	dc.b 0, 2, 8, $BA, $B8, $BB	
SME_JtC8o_FA0:	dc.b 0, 2, $88, $C7, $58, $D0	
SME_JtC8o_FA6:	dc.b 0, 3, $88, $D6, $38, $DF, $28, $E3	
SME_JtC8o_FAE:	dc.b 0, 1, $B8, $E6	
SME_JtC8o_FB2:	dc.b 0, 2, $B8, $F2, $28, $FE	
SME_JtC8o_FB8:	dc.b 0, 3, $29, 1, $19, 4, $B9, 6	
SME_JtC8o_FC0:	dc.b 0, 1, $B9, $12	
SME_JtC8o_FC4:	dc.b 0, 2, 9, $1E, $B9, $1F	
SME_JtC8o_FCA:	dc.b 0, 2, $B9, $2B, $29, $37	
SME_JtC8o_FD0:	dc.b 0, 4, $19, $3A, $39, $3C, $59, $40, $19, $46	
SME_JtC8o_FDA:	dc.b 0, 3, 9, $48, $79, $49, $29, $51	
SME_JtC8o_FE2:	dc.b 0, 3, 9, $54, $79, $55, $29, $5D	
SME_JtC8o_FEA:	dc.b 0, 2, $59, $60, $B9, $66	
SME_JtC8o_FF0:	dc.b 0, 3, 9, $72, $89, $73, $39, $7C	
SME_JtC8o_FF8:	dc.b 0, 3, $19, $80, $59, $82, $39, $88	
SME_JtC8o_1000:	dc.b 0, 3, $59, $8C, $39, $92, $19, $96	
SME_JtC8o_1008:	dc.b 0, 2, 9, $98, $B9, $99	
SME_JtC8o_100E:	dc.b 0, 4, $19, $A5, 9, $A7, 9, $A8, $B9, $A9	
SME_JtC8o_1018:	dc.b 0, 3, $19, $B5, $B9, $B7, $29, $C3	
SME_JtC8o_1020:	dc.b 0, 3, $19, $C6, $79, $C8, $29, $D0	
SME_JtC8o_1028:	dc.b 0, 2, 9, $D3, $89, $D4	
SME_JtC8o_102E:	dc.b 0, 3, $29, $DD, $79, $E0, $29, $E8	
SME_JtC8o_1036:	dc.b 0, 1, $B9, $EB	
SME_JtC8o_103A:	dc.b 0, 4, $39, $F7, $29, $FB, $79, $FE, $7A, 6	
SME_JtC8o_1044:	dc.b 0, 4, $1A, $E, $BA, $10, $5A, $1C, $2A, $22	
SME_JtC8o_104E:	dc.b 0, 3, $2A, $25, $BA, $28, $7A, $34	
SME_JtC8o_1056:	dc.b 0, 4, $2A, $3C, $1A, $3F, $1A, $41, $FA, $43	
SME_JtC8o_1060:	dc.b 0, 4, $39, $F7, $29, $FB, $79, $FE, $7A, 6	
SME_JtC8o_106A:	dc.b 0, 4, $1A, $E, $BA, $10, $5A, $1C, $2A, $22	
SME_JtC8o_1074:	dc.b 0, 3, $2A, $25, $BA, $28, $7A, $34	
SME_JtC8o_107C:	dc.b 0, 4, $2A, $3C, $1A, $3F, $1A, $41, $FA, $43	
SME_JtC8o_1086:	dc.b 0, 4, $BA, $53, $A, $5F, $1A, $60, $5A, $62	
SME_JtC8o_1090:	dc.b 0, 4, $3A, $68, $7A, $6C, $3A, $74, $2A, $78	
SME_JtC8o_109A:	dc.b 0, 3, $2A, $7B, $3A, $7E, $BA, $82	
SME_JtC8o_10A2:	dc.b 0, 4, $A, $8E, $7A, $8F, $2A, $97, $5A, $9A	
SME_JtC8o_10AC:	dc.b 0, 3, $BA, $A0, $5A, $AC, $1A, $B2	
SME_JtC8o_10B4:	dc.b 0, 2, $BA, $B4, $5A, $C0	
SME_JtC8o_10BA:	dc.b 0, 3, $2A, $C6, $7A, $C9, $2A, $D1	
SME_JtC8o_10C2:	dc.b 0, 2, $BA, $D4, $2A, $E0	
SME_JtC8o_10C8:	dc.b 0, 2, $BA, $E3, $2A, $E0	
SME_JtC8o_10CE:	dc.b 0, 2, $BA, $EF, $2A, $E0	
SME_JtC8o_10D4:	dc.b 0, 2, $BA, $FB, $2A, $E0	
SME_JtC8o_10DA:	dc.b 0, 2, $BB, 7, $2A, $E0	
SME_JtC8o_10E0:	dc.b 0, 3, $BB, $13, $2B, $1F, $2B, $22	
SME_JtC8o_10E8:	dc.b 0, 3, $BB, $25, $2B, $1F, $2B, $22	
SME_JtC8o_10F0:	dc.b 0, 2, $B7, $EF, $37, $FB	
SME_JtC8o_10F6:	dc.b 0, 3, $3B, $31, $3B, $35, $8B, $39	
SME_JtC8o_10FE:	dc.b 0, 4, $5B, $42, $3B, $48, $2B, $4C, $5B, $4F	
SME_JtC8o_1108:	dc.b 0, 4, $5B, $55, $3B, $5B, $2B, $5F, $7B, $62	
SME_JtC8o_1112:	dc.b 0, 4, $2B, $6A, $BB, $6D, $B, $79, $1B, $7A	
SME_JtC8o_111C:	dc.b 0, 2, $2B, $6A, $BB, $7C	
SME_JtC8o_1122:	dc.b 0, 5, $7B, $88, $1B, $93, $5B, $95, $3B, $9B, $2B, $9F	
SME_JtC8o_112E:	dc.b 0, 5, $2B, $90, $1B, $93, $5B, $95, $3B, $9B, $2B, $9F	
SME_JtC8o_113A:	dc.b 0, 1, $FB, $A2	
SME_JtC8o_113E:	dc.b 0, 1, $FB, $B2	
SME_JtC8o_1142:	dc.b 0, 1, $FB, $C2	
SME_JtC8o_1146:	dc.b 0, 1, $FB, $D2	
SME_JtC8o_114A:	dc.b 0, 1, $FB, $E2	
SME_JtC8o_114E:	dc.b 0, 2, $FF, $C3, $F, $D3	
SME_JtC8o_1154:	dc.b 0, 2, $FF, $C3, $F, $D3	
SME_JtC8o_115A:	dc.b 0, 4, $8C, $C, $1C, $15, $3C, $17, $2C, $1B	
SME_JtC8o_1164:	dc.b 0, 4, $8C, $C, $1C, $1E, $3C, $20, $2C, $24	
SME_JtC8o_116E:	dc.b 0, 4, $2C, $27, $7C, $2A, $2C, $32, $1C, $35	
SME_JtC8o_1178:	dc.b 0, 3, $2C, $37, $BC, $3A, $2C, $46	
SME_JtC8o_1180:	dc.b 0, 4, $3E, $EA, $3E, $EE, $7E, $F2, $5E, $FA	
SME_JtC8o_118A:	dc.b 0, 4, $3E, $EA, $3E, $EE, $7F, 0, $5F, 8	
SME_JtC8o_1194:	dc.b 0, 4, $3E, $EA, $3E, $EE, $7F, $E, $5F, $16	
SME_JtC8o_119E:	dc.b 0, 4, $3E, $EA, $3E, $EE, $7E, $F2, $5E, $FA	
SME_JtC8o_11A8:	dc.b 0, 4, $3E, $EA, $3E, $EE, $7F, 0, $5F, 8	
SME_JtC8o_11B2:	dc.b 0, 4, $3E, $EA, $3E, $EE, $7F, $E, $5F, $16	
SME_JtC8o_11BC:	dc.b 0, 3, $2C, $BC, $FC, $BF, $C, $CF	
SME_JtC8o_11C4:	dc.b 0, 3, $2C, $D0, $FC, $D3, $C, $CF	
SME_JtC8o_11CC:	dc.b 0, 3, $1C, $E3, $FC, $E5, $C, $CF	
SME_JtC8o_11D4:	dc.b 0, 3, $BC, $F5, $D, 1, $5D, 2	
SME_JtC8o_11DC:	dc.b 0, 3, $BD, 8, $D, $14, $5D, $15	
SME_JtC8o_11E4:	dc.b 0, 4, $FD, $1B, $D, $2B, $1D, $2C, $1D, $2E	
SME_JtC8o_11EE:	dc.b 0, 4, $2D, $30, $7D, $33, $1D, $3B, $2D, $3D	
SME_JtC8o_11F8:	dc.b 0, 4, $2D, $30, $7D, $40, $1D, $3B, $2D, $3D	
SME_JtC8o_1202:	dc.b 0, 4, $2D, $48, $3D, $4B, $7D, $4F, $2D, $3D	
SME_JtC8o_120C:	dc.b 0, 2, $7D, $76, $8D, $7E	
SME_JtC8o_1212:	dc.b 0, 3, $2D, $87, $7D, $8A, $5D, $92	
SME_JtC8o_121A:	dc.b 0, 3, $2D, $87, $7D, $98, $5D, $92	
SME_JtC8o_1222:	dc.b 0, 3, $2D, $87, $7D, $A0, $5D, $92	
SME_JtC8o_122A:	dc.b 0, 3, $BD, $A8, $2D, $B4, $3D, $B7	
SME_JtC8o_1232:	dc.b 0, 3, $BD, $BB, $2D, $C7, $3D, $CA	
SME_JtC8o_123A:	dc.b 0, 3, $2F, $6E, $FF, $71, $3F, $81	
SME_JtC8o_1242:	dc.b 0, 3, $2F, $85, $FF, $88, $2F, $98	
SME_JtC8o_124A:	dc.b 0, 3, $F, $9B, $FF, $9C, $3F, $AC	
SME_JtC8o_1252:	dc.b 0, 3, $2F, $85, $FF, $B0, $2F, $C0	
SME_JtC8o_125A:	dc.b 0, 4, $8E, 1, $7F, $D4, $2F, $DC, $F, $DF	
SME_JtC8o_1264:	dc.b 0, 4, $8E, 1, $7F, $E0, $2F, $E8, $F, $EB	
SME_JtC8o_126E:	dc.b 0, 4, $8E, 1, $7F, $D4, $2F, $DC, $F, $DF	
SME_JtC8o_1278:	dc.b 0, 4, $8E, 1, $7F, $E0, $2F, $E8, $F, $EB	
SME_JtC8o_1282:	dc.b 0, 4, $8E, 1, $7F, $D4, $2F, $DC, $F, $DF	
SME_JtC8o_128C:	dc.b 0, 4, $8E, 1, $7F, $E0, $2F, $E8, $F, $EB	
SME_JtC8o_1296:	dc.b 0, 4, $8E, 1, $7F, $D4, $2F, $DC, $F, $DF	
SME_JtC8o_12A0:	dc.b 0, 4, $8E, 1, $7F, $E0, $2F, $E8, $F, $EB	
SME_JtC8o_12AA:	dc.b 0, 4, $8E, 1, $7F, $D4, $2F, $DC, $F, $DF	
SME_JtC8o_12B4:	dc.b 0, 4, $2E, $61, $7E, $64, $2E, $6C, $2E, $6F	
SME_JtC8o_12BE:	dc.b 0, 4, $2E, $72, $3E, $75, $3E, $79, $5E, $7D	
SME_JtC8o_12C8:	dc.b 0, 4, $8E, 1, $7F, $E0, $2F, $E8, $F, $EB	
SME_JtC8o_12D2:	dc.b 0, 4, $8E, 1, $7F, $D4, $2F, $DC, $F, $DF	
SME_JtC8o_12DC:	dc.b 0, 4, $8E, 1, $7F, $E0, $2F, $E8, $F, $EB	
SME_JtC8o_12E6:	dc.b 0, 3, $8E, $B0, $1E, $B9, $2E, $BB	
SME_JtC8o_12EE:	dc.b 0, 3, $5E, $BE, $3E, $C4, $E, $C8	
SME_JtC8o_12F6:	dc.b 0, 2, $5E, $C9, $3E, $CF	
SME_JtC8o_12FC:	dc.b 0, 3, $5E, $BE, $3E, $C4, $E, $C8	
SME_JtC8o_1304:	dc.b 0, 3, $8E, $B0, $1E, $B9, $2E, $BB	
SME_JtC8o_130C:	dc.b 0, 2, $BE, $D3, $E, $DF	
SME_JtC8o_1312:	dc.b 0, 2, $5E, $E0, $3E, $E6	
SME_JtC8o_1318:	dc.b 0, 2, $BE, $D3, $E, $DF	
SME_JtC8o_131E:	dc.b 0, 3, $BB, $13, $2B, $1F, $2B, $22	
SME_JtC8o_1326:	dc.b 0, 3, $BB, $25, $2B, $1F, $2B, $22	
SME_JtC8o_132E:	dc.b 0, 1, $BF, $1C	
SME_JtC8o_1332:	dc.b 0, 3, $5F, $28, $7F, $2E, $5F, $36	
SME_JtC8o_133A:	dc.b 0, 2, $FF, $3C, $5F, $4C	
SME_JtC8o_1340:	dc.b 0, 3, $8F, $52, $3F, $5B, $5F, $5F	
SME_JtC8o_1348:	dc.b 0, 3, $8F, $65, $3F, $5B, $5F, $5F	
SME_JtC8o_1350:	dc.b 0, 3, $BB, $13, $2B, $1F, $2B, $22	
SME_JtC8o_1358:	dc.b 0, 3, $BB, $25, $2B, $1F, $2B, $22	
SME_JtC8o_1360:	dc.b 0, 4, $BD, $57, $D, $63, $3D, $64, $2D, $68	
SME_JtC8o_136A:	dc.b 0, 3, $50, 0, $70, 6, $10, $E	
		even